# Requisitos

[node](https://nodejs.org) >= 18.16
[PostgreSQL](https://www.postgresql.org/download/)

# Instalação
Clonar o repositório:
```bash
$ git clone https://gitlab.com/TiLopes/pap_backend
```
Instalar as bibliotecas:
```bash
$ yarn install
```
Alterar nome do ficheiro `.env-example` para `.env` e preencher as variáveis:
```env
ACCESS_TOKEN_SECRET=
PORT=
```
Alterar nome da pasta `config-example` para `config` e preencher as variáveis :
```typescript
// config/index.ts
const config = {
  development: {
    EMAIL_SENDER: "",
    EMAIL_PASS: "",
    SMTP_SERVER: "",
    SMTP_PORT: 0,
    API_SERVER: "",
  },
  production: {
    EMAIL_SENDER: "",
    EMAIL_PASS: "",
    SMTP_SERVER: "",
    SMTP_PORT: 0,
    API_SERVER: "",
  },
};
...
```
Alterar nome do ficheiro `models/index-example.ts` para `models/index.ts` e preencher as propriedades:
```typescript
...
const db: Sequelize = new Sequelize({
  username: "",
  password: "",
  database: "",
  schema: "",
  host: "",
  dialect: "",
  define: {
    timestamps: false,
  },
});
...
```
Se o backend estiver em um domínio separado do frontend, fazer a seguinte alteração no ficheiro `index.ts`:
```typescript
app.use(
  cors({
    origin: ["domínio do front-end e porta se não for a 80"],
	...
  }),
);
...
```
# Iniciar a API
Converter ficheiros TypeScript para JavaScript e iniciar a API:
```bash
$ yarn run start
```