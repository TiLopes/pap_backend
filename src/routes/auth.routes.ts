import { Router } from "express";
import { loginGET, loginPOST, logout, signupPOST } from "../controllers/auth.controller";
import jwt, { VerifyErrors } from "jsonwebtoken";
import verifyJWT from "../middlewares/verifyJWT";

const router: Router = Router();

router.post("/api/auth/signup", signupPOST);

/* Rota tipo GET */
router.get("/api/auth/login", loginGET);

/* Rota tipo POST */
router.post("/api/auth/login", loginPOST);

router.post("/api/verify/group", (req, res) => {
  const { group, token } = req.body;

  if (!group) {
    return res.sendStatus(401);
  }

  if (!token) {
    return res.sendStatus(401);
  }

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET as string, async (err: VerifyErrors | null, decodedToken: any) => {
    if (err) {
      console.error(err);
      res.status(403).json({ error: "Token is invalid" });
      return;
    }

    if (decodedToken.id_grupo != group) {
      res.status(403).json({ error: "Token is invalid" });
      return;
    }
  });

  res.sendStatus(200);
});

router.post("/api/auth/logout", verifyJWT, logout);

export default router;
