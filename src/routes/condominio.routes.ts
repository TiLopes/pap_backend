import { Router } from "express";
import { condominioInfoGet } from "../controllers/condominio.controller";
import verifyJWT from "../middlewares/verifyJWT";
import verifyPermission from "../middlewares/verifyPermissions";

const router = Router();

router.get("/condominio/info", verifyJWT, verifyPermission, condominioInfoGet);

export default router;
