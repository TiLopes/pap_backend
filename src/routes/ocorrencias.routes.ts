import { Router } from "express";
import verifyJWT from "../middlewares/verifyJWT";
import verifyPermission from "../middlewares/verifyPermissions";
import {
  createOcorrencia,
  createOcorrenciaGET,
  filterOcorrencias,
  getOcorrencia,
  getOcorrencias,
  imprimirOcorrencias,
  solveOcorrencia,
} from "../controllers/ocorrencias.controller";

const router = Router();

router.get("/create/ocorrencia", verifyJWT, verifyPermission, createOcorrenciaGET);
router.post("/create/ocorrencia", verifyJWT, verifyPermission, createOcorrencia);
router.get("/get/ocorrencias", verifyJWT, verifyPermission, getOcorrencias);
router.get("/get/ocorrencia/:id", verifyJWT, verifyPermission, getOcorrencia);
router.post("/solve/ocorrencia/:id", verifyJWT, verifyPermission, solveOcorrencia);
router.get("/filter/ocorrencias", verifyJWT, verifyPermission, filterOcorrencias);
router.get("/imprimir/ocorrencias", verifyJWT, verifyPermission, imprimirOcorrencias);

export default router;
