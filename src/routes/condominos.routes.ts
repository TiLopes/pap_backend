import { Router } from "express";
import verifyJWT from "../middlewares/verifyJWT";
import verifyPermission from "../middlewares/verifyPermissions";
import { banCondomino, createCondomino, getCondominos, getMe } from "../controllers/condominos.controller";

const router = Router();

router.put("/create/condomino", verifyJWT, verifyPermission, createCondomino);
router.get("/get/condominos", verifyJWT, verifyPermission, getCondominos);
router.get("/get/condominos/:type", verifyJWT, verifyPermission, getCondominos);
router.get("/get/me", verifyJWT, verifyPermission, getMe);
router.post("/ban/condomino", verifyJWT, verifyPermission, banCondomino);

export default router;
