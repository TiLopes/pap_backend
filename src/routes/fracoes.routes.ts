import { Router } from "express";
import verifyJWT from "../middlewares/verifyJWT";
import verifyPermission from "../middlewares/verifyPermissions";
import {
  createFracao,
  deleteFracao,
  editFracao,
  getFracao,
  getFracoes,
  getFracoesCondomino,
  getFracoesLivres,
  getTiposFracoes,
} from "../controllers/fracoes.controller";

const router = Router();

router.post("/create/fracao", verifyJWT, verifyPermission, createFracao);
router.get("/get/fracoeslivres", verifyJWT, verifyPermission, getFracoesLivres);
router.get("/get/fracoes", verifyJWT, verifyPermission, getFracoes);
router.get("/get/fracao/:id", verifyJWT, verifyPermission, getFracao);
router.post("/edit/fracao/:id", verifyJWT, verifyPermission, editFracao);
router.get("/my/fracoes", verifyJWT, verifyPermission, getFracoesCondomino);
router.get("/tipos/fracoes", verifyJWT, verifyPermission, getTiposFracoes);
router.post("/delete/fracao", verifyJWT, verifyPermission, deleteFracao);

export default router;
