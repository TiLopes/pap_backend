import { Router } from "express";
import verifyJWT from "../middlewares/verifyJWT";
import verifyPermission from "../middlewares/verifyPermissions";
import { addMovimento, getMovimento, getMovimentos } from "../controllers/movimentos.controller";

const router = Router();

/**
 * 1. caminho
 * 2. middleware
 * 3. middleware
 * 4. controller
 */
router.post("/add/movimento", verifyJWT, verifyPermission, addMovimento);
router.get("/get/movimentos", verifyJWT, verifyPermission, getMovimentos);
router.get("/get/movimento/:id", verifyJWT, verifyPermission, getMovimento);

export default router;
