import { Request, Response, Router } from "express";
import verifyJWT from "../middlewares/verifyJWT";
import verifyPermission from "../middlewares/verifyPermissions";

const router: Router = Router();

router.get("/api/checks", verifyJWT, verifyPermission, (_req: Request, res: Response) => {
  res.sendStatus(200).json({});
});

export default router;
