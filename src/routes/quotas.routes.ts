import { Router } from "express";
import verifyJWT from "../middlewares/verifyJWT";
import verifyPermission from "../middlewares/verifyPermissions";
import {
  addQuota,
  desativarQuota,
  getCustomQuotas,
  getPagamentosQuotas,
  getQuotas,
  myPagamentosQuotas,
  myQuotas,
  pagarQuota,
  quotaExists,
} from "../controllers/quotas.controller";

const router = Router();

router.post("/pagar/quota", verifyJWT, verifyPermission, pagarQuota);
router.get("/get/pagamentos_quotas", verifyJWT, verifyPermission, getPagamentosQuotas);
router.get("/my/pagamentos_quotas", verifyJWT, verifyPermission, myPagamentosQuotas);
router.post("/desativar/quota", verifyJWT, verifyPermission, desativarQuota);
router.get("/my/quotas", verifyJWT, verifyPermission, myQuotas);
router.post("/add/quota", verifyJWT, verifyPermission, addQuota);
router.get("/add/quota", verifyJWT, verifyPermission, quotaExists);
router.get("/get/quotas", verifyJWT, verifyPermission, getQuotas);
router.get("/get/quotas/:type", verifyJWT, verifyPermission, getCustomQuotas);

export default router;
