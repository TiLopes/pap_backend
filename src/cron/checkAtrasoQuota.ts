import { format } from "date-fns";
import db from "../models";
import { initModels } from "../models/init-models";
import type { Condominio as CondominioType } from "../models/init-models";
import { Op, Sequelize, WhereOptions } from "sequelize";
import { createTransport } from "nodemailer";
import config from "../config";
import pt from "date-fns/locale/pt";

const { Quota, PagamentosQuotas, Condomino, Fracoes, Condominio } = initModels(db);

export async function checkAtrasoQuota() {
  const data = new Date();

  try {
    // condominios que têm pagamento de quotas nesse dia
    const condominios = await Condominio.findAll({
      where: {
        dia_pagamento_quota: data.getDate(),
      },
      attributes: ["id", "nome"],
    });

    // se não houver condomínios parar de executar
    if (condominios.length === 0) {
      return;
    }

    // obter todas as quotas pagas
    const pagamentos = await PagamentosQuotas.findAll({
      attributes: ["id"],
    });

    // para cada condominio obter as quotas por pagar e enviar email aos condóminos
    for (const condominio of condominios) {
      // buscar quota por pagar à base de dados
      const quotasPagarRAW = await Quota.findAll({
        where: {
          data_vencimento: {
            [Op.gte]: data,
          },
          data_inicio: {
            [Op.lte]: data,
          },
          id_pagamento: {
            [Op.notIn]: pagamentos.map((q) => q.id),
          },
          id_condominio: condominio.id,
        },
        include: [
          {
            model: Fracoes,
            as: "id_fracao_fraco",
            attributes: ["id_condomino", "andar", "id"],
            where: {
              id_condomino: { [Op.ne]: null },
              id_condominio: condominio.id,
            },
            include: [
              {
                model: Condomino,
                as: "id_condomino_condomino",
                attributes: ["id", "nome_ocupante", "email_ocupante"],
                where: {
                  id_condominio: condominio.id,
                },
              },
            ],
          },
        ],
      });

      // se não houver quotas por pagar
      if (quotasPagarRAW.length === 0) continue;

      const quotas = new Map<number, MapValue>();

      // adicionar cada quota a um Map por condómino
      for (const quota of quotasPagarRAW) {
        const id_condomino = quota.id_fracao_fraco.id_condomino_condomino.id;
        if (quotas.has(id_condomino)) {
          const previousValues = quotas.get(id_condomino)!;
          previousValues.fracoes = [
            ...previousValues.fracoes,
            { id: quota.id_fracao_fraco.id, valor: quota.valor, periodo: new Date(quota.data_vencimento as string) },
          ];
        } else {
          quotas.set(id_condomino, {
            fracoes: [
              {
                id: quota.id_fracao_fraco.id,
                valor: quota.valor,
                periodo: new Date(quota.data_vencimento as string),
              },
            ],
            nome_ocupante: quota.id_fracao_fraco.id_condomino_condomino.nome_ocupante,
            condominio: condominio,
            email_ocupante: quota.id_fracao_fraco.id_condomino_condomino.email_ocupante,
          });
        }
      }

      sendAtrasoQuotaEmail(quotas);
    }
  } catch (error) {
    console.error({ error });
  }
}

function sendAtrasoQuotaEmail(quotas: Map<number, MapValue>) {
  const transporter = createTransport({
    host: config.SMTP_SERVER, // host do servidor SMTP
    port: config.SMTP_PORT, // porta do servidor SMTP
    auth: {
      // autenticação no servidor
      user: config.EMAIL_SENDER,
      pass: config.EMAIL_PASS,
    },
  });

  quotas.forEach((value) => {
    let html = `<!doctype html>
    <html>
      <body>
				<p>Caro/a ${value.nome_ocupante},</p>
				<p>Existe${value.fracoes.length > 1 ? "m" : ""} ${value.fracoes.length} quota${
          value.fracoes.length > 1 ? "s" : ""
        } por pagar do condomínio ${value.condominio.nome}.</p>
				<ul>
		`;

    value.fracoes.forEach(
      (fracao) =>
        (html += `<li>Fração ${fracao.id} - ${fracao.valor}€, ${format(fracao.periodo, "MMMM 'de' yyyy", {
          locale: pt,
        })}</li>`),
    );

    html += `</ul><a href="${config.API_SERVER}/condominos/quotas"></a></body></html>`;

    const mailOptions = {
      from: process.env.EMAIL_SENDER, // email do projeto
      to: value.email_ocupante, // email do condómino
      subject: "Aviso - Quotas em atraso", // assunto
      // conteúdo do email em html
      html: html,
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
  });
}

interface MapValue {
  fracoes: { id: string; valor: number; periodo: Date }[];
  nome_ocupante: string;
  email_ocupante: string;
  condominio: CondominioType;
}
