import { Request, RequestHandler, Response } from "express";
import db from "../models";
import { initModels } from "../models/init-models";
import type { Fracoes as FracoesType, Condominio as CondominioType } from "../models/init-models";
import { Op, WhereOptions } from "sequelize";

const { Condominio, Condomino, Fracoes, Quota, PagamentosQuotas, TiposFracao } = initModels(db);

const createFracao = async (req: Request, res: Response) => {
  // retirar a informação do "corpo" do pedido
  const { fracao, andar, escritura, permilagem_rateio, tipoFracao } = req.body;

  try {
    // enviar a informação para o método create
    await Fracoes.create({
      id: fracao,
      tipo: tipoFracao,
      andar: andar.toUpperCase(),
      permilagem_escritura: escritura,
      permilagem_rateio,
      id_condominio: req.condominioID, // id do condomínio que fez o pedido
    });

    res.status(200).json({ success: true });
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false, err });
  }
};

const getFracoesLivres = async (req: Request, res: Response) => {
  let fracoes: FracoesType[];
  let condominio: CondominioType | null;

  try {
    fracoes = await Fracoes.findAll({
      where: { id_condominio: req.condominioID, estado: "Livre" },
      attributes: ["id"],
    });
    condominio = await Condominio.findByPk(req.condominioID, {
      attributes: ["morada", "cod_postal"],
    });
  } catch (err) {
    console.error(err);
    return res.status(400).json({ errors: err });
  }

  res.status(200).json({ fracoes, condominio });
};

const getTiposFracoes: RequestHandler = async (req, res) => {
  try {
    const tipos = await TiposFracao.findAll();
    res.status(200).json({ success: true, tipos });
  } catch (error) {
    console.error(error);
    res.status(400).json({ success: false });
  }
};

const getFracoes = async (req: Request, res: Response) => {
  // parâmetros do pedido
  const { limit, offset, order, dir } = req.query;

  try {
    // procurar e contar o número de frações do condominio
    let fracoesRAW = await Fracoes.findAndCountAll({
      // limitar ao condominio do pedido
      where: { id_condominio: req.condominioID },
      attributes: {
        // excluir colunas
        exclude: ["id_condominio"],
      },
      include: [
        {
          /* incluir a tabela Condomino onde a chave estrangeira
           * da tabela frações é igual à chave primária da
           * tabela condómino
           */
          model: Condomino,
          as: "id_condomino_condomino",
          // apenas obter estas colunas
          attributes: ["nome_ocupante"],
        },
        {
          model: TiposFracao,
          as: "tipo_tipos_fracao",
        },
      ],
      // quantos registos avançar
      offset: parseInt(offset as string, 10),
      // quantos registos serão devolvidos
      limit: parseInt(limit as string, 10),
      // ordenar as colunas (nome coluna, direção)
      order: [[order as string, dir as string]],
      nest: true,
    });

    const fracoes: unknown[] = [];

    // formatação da informação para ser enviada para o cliente
    for (const fracao of fracoesRAW.rows) {
      fracoes.push({
        id_fracao: fracao.id,
        tipo: fracao.tipo_tipos_fracao.descricao,
        andar: fracao.andar,
        permilagem_escritura: fracao.permilagem_escritura,
        estado: fracao.estado,
        condomino: fracao?.id_condomino_condomino ? fracao.id_condomino_condomino.nome_ocupante : "",
        data_aquisicao: fracao.data_aquisicao,
      });
    }

    // envio resposta para o cliente
    res.status(200).json({ success: true, fracoes, count: fracoesRAW.count });
  } catch (err) {
    console.error(err);
    res.status(401).json({ success: false });
  }
};

async function getFracao(req: Request, res: Response) {
  // buscar o id nos parametros do pedido
  const { id } = req.params;

  try {
    // procurar apenas uma fração
    const fracao = await Fracoes.findOne({
      where: {
        id: id, // id da fração é igual ao do pedido
        id_condominio: req.condominioID, // condomínio é igual ao do pedido
      },
      attributes: {
        exclude: ["id_condominio", "id_condomino"],
      },
      include: [
        {
          model: Condomino,
          as: "id_condomino_condomino",
          attributes: ["nome_ocupante", "id"],
        },
        {
          model: TiposFracao,
          as: "tipo_tipos_fracao",
        },
      ],
      nest: true,
    });
    // enviar os detalhes da fração para o cliente
    res.status(200).json({ success: true, fracao });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
}

async function editFracao(req: Request, res: Response) {
  // buscar o id nos parametros do pedido
  const { id } = req.params;
  // buscar a nova informação ao pedido
  const { andar, escritura = 0, rateio, estado, tipo, ocupante = null, data_aquisicao } = req.body;

  try {
    await Fracoes.update(
      // atualizar a fração com os novos dados
      {
        andar,
        permilagem_escritura: escritura,
        permilagem_rateio: rateio,
        estado,
        tipo,
        id_condomino: ocupante,
        data_aquisicao,
      },
      {
        where: {
          id,
          id_condominio: req.condominioID,
        },
      },
    );

    res.status(200).json({ success: true });
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false });
  }
}

export const getFracoesCondomino: RequestHandler = async (req, res) => {
  try {
    // procurar todas as frações associadas ao condomino
    const fracoes = await Fracoes.findAll({
      where: {
        // id do condomínio associado ao pedido
        id_condominio: req.condominioID,
        // id do condómino associado ao pedido
        id_condomino: req.condominoID,
      },
      attributes: {
        exclude: ["id_condomino", "id_condominio"],
      },
      include: [
        {
          model: TiposFracao,
          as: "tipo_tipos_fracao",
        },
      ],
    });

    res.status(200).json({ success: true, fracoes });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

export const deleteFracao: RequestHandler = async (req, res) => {
  // id da fração a eliminar
  const { id }: { id: number } = req.body;

  try {
    // Obter as quotas da fração para depois elimina-las
    const quotas = await Quota.findAll({
      where: { id_fracao: id, id_condominio: req.condominioID },
      attributes: ["id", "id_pagamento"],
    });

    console.log(quotas.map((q) => q.id_pagamento));

    // Eliminar todos as quotas da fração
    await Quota.destroy({ where: { id_fracao: id, id_condominio: req.condominioID } });

    // Eliminar todos os pagamentos da fração
    await PagamentosQuotas.destroy({
      where: {
        id: { [Op.in]: quotas.map((q) => (q.id_pagamento ? q.id_pagamento : -1)) },
      },
    });


    // Eliminar a fração
    await Fracoes.destroy({
      where: {
        id: id,
        id_condominio: req.condominioID,
      },
    });

    res.status(200).json({ success: true });
  } catch (error) {
    console.error(error);
    res.status(400).json({ success: false });
  }
};

export { createFracao, getFracao, getFracoes, getFracoesLivres, editFracao, getTiposFracoes };
