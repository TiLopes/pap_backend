import { RequestHandler } from "express";
import db from "../models";
import { initModels } from "../models/init-models";

const { Condominio } = initModels(db);

export const condominioInfoGet: RequestHandler = async (req, res) => {
  try {
    // procurar o condomínio e excluir informação confidencial
    const condominio = await Condominio.findOne({
      where: {
        id: req.condominioID,
      },
      attributes: {
        exclude: ["password", "auth_token", "id_grupo"],
      },
    });

    // se não existir um condominio devolver um erro
    if (!condominio) {
      return res.status(404).json({ success: false });
    }

    // enviar a informação para o front-end
    res.status(200).json({ success: true, condominio });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ sucess: false });
  }
};
