import { RequestHandler } from "express";
import { initModels } from "../models/init-models";
import db from "../models";

const { Movimentos, Condomino } = initModels(db);

export const getMovimentos: RequestHandler = async (req, res) => {
  const { limit, offset, order, dir } = req.query;

  try {
    const movimentosRAW = await Movimentos.findAndCountAll({
      where: {
        id_condominio: req.condominioID,
      },
      attributes: {
        exclude: ["id_condominio"],
      },
      offset: Number(offset),
      limit: Number(limit),
      order: [[order as string, dir as string]],
      include: [
        {
          model: Condomino,
          as: "id_condomino_condomino",
          attributes: ["nome_ocupante"],
        },
      ],
      nest: true,
    });

    let movimentos: unknown[] = [];

    for (const mov of movimentosRAW.rows) {
      movimentos.push({
        id: mov.id,
        tipo: mov.tipo.charAt(0).toUpperCase() + mov.tipo.slice(1),
        descritivo: mov.descritivo,
        observacoes: mov.observacoes,
        condomino: mov?.id_condomino_condomino ? mov.id_condomino_condomino.nome_ocupante : "",
        data: mov.data,
        valor: mov.valor,
      });
    }

    res.status(200).json({ success: true, movimentos, count: movimentosRAW.count });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

export const addMovimento: RequestHandler = async (req, res) => {
  const {
    descritivo,
    valor,
    data,
    observacoes,
    condomino,
    tipo,
  }: {
    descritivo: string;
    valor: number;
    data: string;
    observacoes: string;
    condomino: number;
    tipo: string;
  } = req.body;

  try {
    const count = await Movimentos.count({
      where: {
        id_condominio: req.condominioID,
      },
    });

    console.info({ count });

    await Movimentos.create({
      id: count + 1,
      descritivo,
      data,
      valor,
      observacoes,
      id_condomino: condomino,
      tipo,
      id_condominio: req.condominioID,
    });

    res.status(200).json({ success: true });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

export const getMovimento: RequestHandler = async (req, res) => {
  const { id } = req.params;

  try {
    const movimentoRAW = await Movimentos.findOne({
      where: {
        id_condominio: req.condominioID,
        id,
      },
      include: [
        {
          model: Condomino,
          as: "id_condomino_condomino",
          attributes: ["nome_ocupante"],
        },
      ],
      nest: true,
    });

    if (!movimentoRAW) {
      res.status(400).json({ success: false });
      return;
    }

    const movimento = {
      id: movimentoRAW.id,
      tipo: movimentoRAW.tipo.charAt(0).toUpperCase() + movimentoRAW.tipo.slice(1),
      descritivo: movimentoRAW.descritivo,
      observacoes: movimentoRAW.observacoes,
      condomino: movimentoRAW?.id_condomino_condomino ? movimentoRAW.id_condomino_condomino.nome_ocupante : "",
      data: movimentoRAW.data,
      valor: movimentoRAW.valor,
    };

    res.status(200).json({ success: true, movimento });
  } catch (err) {
    console.error({ err });

    res.status(400).json({ success: false });
  }
};
