import { RequestHandler } from "express";
import { QuotaCreationAttributes, initModels } from "../models/init-models";
import db from "../models";
import { Op } from "sequelize";
import { addMonths, differenceInCalendarMonths, endOfMonth, format, getDate, setDate, startOfMonth } from "date-fns";

const { Quota, Fracoes, Condominio, Condomino, PagamentosQuotas } = initModels(db);

export const getQuotas: RequestHandler = async (req, res) => {
  const { limit, offset, order, dir } = req.query; // parâmetros do pedido

  try {
    // procurar e contar o número de quotas do condominio
    const quotas = await Quota.findAndCountAll({
      where: { id_condominio: req.condominioID },
      attributes: { exclude: ["id_condominio"] },
      offset: Number(offset), // quantos registos avançar
      limit: Number(limit), // quantos registos serão retornadas
      order: [
        // ordenar
        ["ativa", "DESC"],
        [order as string, dir as string],
      ],
      nest: true,
    });

    res.status(200).json({ success: true, quotas: quotas.rows, count: quotas.count });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

export const myQuotas: RequestHandler = async (req, res) => {
  const { limit, offset, order, dir } = req.query; // parâmetros do pedido

  try {
    // procurar e contar o número de quotas do condómino
    const quotas = await Quota.findAndCountAll({
      where: { id_condominio: req.condominioID },
      attributes: { exclude: ["id_condominio"] },
      // apenas procurar as quotas onde as frações estão associadas ao condómino
      include: [
        {
          model: Fracoes,
          as: "id_fracao_fraco",
          where: {
            id_condomino: req.condominoID,
            id_condominio: req.condominioID,
          },
          attributes: [],
        },
      ],
      offset: Number(offset), // quantos registos avançar
      limit: Number(limit), // quantos registos serão retornadas
      // ordenar
      order: [
        ["ativa", "DESC"],
        [order as string, dir as string],
      ],
      nest: true,
    });

    res.status(200).json({ success: true, quotas: quotas.rows, count: quotas.count });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

export const getCustomQuotas: RequestHandler = async (req, res) => {
  const { type } = req.params;
  const { id_condomino } = req.query;
  let conditions: any = { [Op.and]: [] };

  if (!id_condomino) {
    res.status(400).json({ success: false });
    return;
  }

  try {
    const fracoes = await Fracoes.findAll({
      where: {
        id_condomino: Number(id_condomino),
        id_condominio: req.condominioID,
      },
    });

    let fracoesOR: any = { [Op.or]: [] };

    for (const f of fracoes) {
      fracoesOR[Op.or].push({ id_fracao: { [Op.eq]: f.id } });
    }

    switch (type) {
      case "ativas":
        conditions[Op.and].push({ ativa: { [Op.eq]: true } });
        break;
    }

    const quotas = await Quota.findAll({
      where: {
        ...conditions,
        id_condominio: req.condominioID,
        ...fracoesOR,
      },
    });

    res.status(200).json({ success: true, quotas });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

export const addQuota: RequestHandler = async (req, res) => {
  const {
    data_vencimento: data_vencimentoRAW,
    data_inicio: data_inicioRAW,
    porPermilagem,
    valor,
    frequencia,
  }: {
    data_vencimento: string;
    data_inicio: string;
    porPermilagem: boolean;
    valor: number;
    frequencia: string;
  } = req.body;

  // formatar a data para o PostgreSQL
  const data_vencimento = format(new Date(data_vencimentoRAW), "yyyy-MM-dd");

  // formatar a data para o PostgreSQL
  const data_inicio = format(new Date(data_inicioRAW), "yyyy-MM-dd");

  try {
    // procurar todas as frações do condominio
    const fracoes = await Fracoes.findAll({
      where: { id_condominio: req.condominioID },
      attributes: ["id", "permilagem_escritura"],
    });

    // obter o orçamento anual do condominio
    const condominio = await Condominio.findByPk(req.condominioID, { attributes: ["orcamento_anual"] });

    // para não sobrepor quotas desativar as quotas no mesmo período
    await Quota.update(
      { ativa: false },
      {
        where: {
          ativa: true,
          [Op.and]: { data_inicio: { [Op.gte]: data_inicio }, data_vencimento: { [Op.lte]: data_vencimento } },
          id_condominio: req.condominioID,
        },
      },
    );
    const quotasCreate: QuotaCreationAttributes[] = [];

    let diferencasMeses = differenceInCalendarMonths(new Date(data_vencimento), new Date(data_inicio));

    for (let i = 0; i <= diferencasMeses; i++) {
      for (const fracao of fracoes) {
        let valor_por_fracao: number = 0.0;

        // verificar se o valor é definido através da permilagem
        if (porPermilagem) {
          valor_por_fracao = Number(condominio!.orcamento_anual) * Number(fracao.permilagem_escritura / 1000);
        } else {
          valor_por_fracao = valor;
        }

        // adicionar ao array das quotas
        quotasCreate.push({
          valor: valor_por_fracao,
          data_vencimento:
            // definir fim da quota como último dia do mês para os meses entre inicio e fim
            i === diferencasMeses
              ? data_vencimento
              : format(
                  setDate(
                    addMonths(new Date(data_inicio), i),
                    getDate(endOfMonth(addMonths(new Date(data_inicio), i))),
                  ),
                  "yyyy-MM-dd",
                ),
          frequencia: frequencia,
          id_condominio: req.condominioID,
          id_fracao: fracao.id,
          ativa: true,
          data_inicio:
            i === 0
              ? data_inicio
              : format(
                  setDate(
                    addMonths(new Date(data_inicio), i),
                    getDate(startOfMonth(addMonths(new Date(data_inicio), i))),
                  ),
                  "yyyy-MM-dd",
                ),
        });
      }
    }

    // utilizar o bulkCreate pois tem melhor performance quando se insere vários registos ao mesmo tempo
    const quotas = await Quota.bulkCreate(quotasCreate);

    res.status(200).json({ success: true });
  } catch (error) {
    console.error(error);
    res.status(400).json({ success: false });
  }
};

export const quotaExists: RequestHandler = async (req, res) => {
  const today = format(new Date(), "yyyy-MM-dd");

  try {
    const quotas = await Quota.count({
      where: {
        [Op.and]: {
          data_vencimento: { [Op.gte]: today },
          data_inicio: { [Op.lte]: today },
        },
        id_condominio: req.condominioID,
      },
    });

    console.info({ quotas });

    res.status(200).json({ success: true, exists: quotas > 0 ? true : false });
  } catch (error) {
    console.error({ error });
    res.status(400).json({ success: false });
  }
};

export const pagarQuota: RequestHandler = async (req, res) => {
  const { data, observacoes, quotas, condomino } = req.body;

  try {
    // auto incrementar por cada pagamento
    const count = (await PagamentosQuotas.count()) + 1;

    const pagamento = await PagamentosQuotas.create({
      id: count,
      observacoes: observacoes,
      data,
      id_condomino: condomino,
    });

    // para todas as quotas criar um pagamento com o mesmo id
    for (const quota of quotas) {
      // atualizar o estado da quota para inativa
      await Quota.update(
        {
          ativa: false,
          id_pagamento: pagamento.id,
        },
        {
          where: {
            id: quota,
            id_condominio: req.condominioID,
          },
        },
      );
    }

    res.status(200).json({ success: true });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

export const getPagamentosQuotas: RequestHandler = async (req, res) => {
  const { limit, offset, order, dir } = req.query; // parâmetros do pedido

  try {
    const pagamentosRAW = await Quota.findAndCountAll({
      where: {
        id_condominio: req.condominioID,
      },
      offset: Number(offset), // quantos registos avançar
      limit: Number(limit), // quantos registos serão retornadas
      // ordenar
      order: [[order as string, dir as string]],
      include: [
        {
          model: PagamentosQuotas,
          as: "id_pagamento_pagamentos_quota",
          required: true,
        },
      ],
      nest: true,
    });

    console.log(pagamentosRAW.count);

    // quantida de pagamentos no condominio
    const count = pagamentosRAW.count;
    const pagamentos_quotas: unknown[] = [];

    // formatar a informação para o envio
    for (const quota of pagamentosRAW.rows) {
      const condomino = await PagamentosQuotas.findOne({
        attributes: [],
        where: [
          {
            id: quota.id_pagamento,
          },
        ],
        include: [
          {
            model: Condomino,
            as: "id_condomino_condomino",
            attributes: ["nome_ocupante"],
          },
        ],
      });

      pagamentos_quotas.push({
        id_pagamento: quota.id_pagamento_pagamentos_quota.id,
        data: quota.id_pagamento_pagamentos_quota.data,
        observacoes: quota.id_pagamento_pagamentos_quota.observacoes,
        fracao: quota.id_fracao,
        periodo: quota.data_vencimento,
        valor: quota.valor,
        condomino: condomino?.id_condomino_condomino.nome_ocupante,
      });
    }

    console.log({ pagamentos_quotas });

    res.status(200).json({ success: true, pagamentos_quotas, count });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

export const myPagamentosQuotas: RequestHandler = async (req, res) => {
  const { limit, offset, order, dir } = req.query; // parâmetros do pedido

  try {
    // procurar e contar todos os pagamentos de quotas do condómino

    const pagamentosRAW = await Quota.findAndCountAll({
      where: {
        id_condominio: req.condominioID,
      },
      offset: Number(offset), // quantos registos avançar
      limit: Number(limit), // quantos registos serão retornadas
      // ordenar
      order: [[order as string, dir as string]],
      include: [
        {
          model: PagamentosQuotas,
          as: "id_pagamento_pagamentos_quota",
          required: true,
          where: {
            id_condomino: req.condominoID,
          },
        },
      ],
      nest: true,
    });

    const count = pagamentosRAW.count;
    const pagamentos_quotas = new Array();

    if (pagamentosRAW.rows.length === 0) {
      res.status(200).json({ success: true, pagamentos_quotas, count });
      return;
    }

    const condomino = await PagamentosQuotas.findOne({
      attributes: [],
      where: [
        {
          id: pagamentosRAW.rows[0].id_pagamento,
        },
      ],
      include: [
        {
          model: Condomino,
          as: "id_condomino_condomino",
          attributes: ["nome_ocupante"],
        },
      ],
    });

    for (const quota of pagamentosRAW.rows) {
      pagamentos_quotas.push({
        id_pagamento: quota.id_pagamento_pagamentos_quota.id,
        data: quota.id_pagamento_pagamentos_quota.data,
        observacoes: quota.id_pagamento_pagamentos_quota.observacoes,
        fracao: quota.id_fracao,
        periodo: quota.data_vencimento,
        valor: quota.valor,
        condomino: condomino?.id_condomino_condomino.nome_ocupante,
      });
    }

    res.status(200).json({ success: true, pagamentos_quotas, count });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

export const desativarQuota: RequestHandler = async (req, res) => {
  const { id }: { id: number } = req.body;

  try {
    await Quota.update(
      {
        ativa: false,
      },
      {
        where: {
          id: id,
          id_condominio: req.condominioID,
        },
      },
    );
    res.status(200).json({ success: true });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};
