import crypto from "crypto";
import { Request, RequestHandler, Response } from "express";
import jwt, { JwtPayload, VerifyErrors } from "jsonwebtoken";
import { createHash } from "node:crypto";
import db from "../models";
import { initModels } from "../models/init-models";
import { LoginErrorCondominio } from "../models/condominio";
import { LoginErrorCondomino } from "../models/condomino";
const { Condominio, Condomino } = initModels(db);

interface LoginError extends Record<string, any> {
  password: string;
}

function handleLoginErrors(err) {
  let errors: LoginError = { password: "" };

  switch (err.message) {
    case LoginErrorCondominio.EMAIL_EMPTY:
      errors.email = "Email vazio.";
      break;
    case LoginErrorCondominio.PASS_EMPTY:
      errors.password = "Password vazia.";
      break;
    case LoginErrorCondominio.USER_NOT_EXISTS:
      errors.email = "Conta não existe";
      break;
    case LoginErrorCondominio.PASS_WRONG:
      errors.password = "Password errada.";
      break;
    case LoginErrorCondomino.USER_BANNED:
      errors.email = "Utilizador banido do condomínio.";
      break;
    case LoginErrorCondomino.USERNAME_EMPTY:
      errors.email = "Nome de utilizador vazio.";
      break;
    case LoginErrorCondomino.PASS_EMPTY:
      errors.password = "Password vazia.";
      break;
    case LoginErrorCondomino.USER_NOT_EXISTS:
      errors.email = "Conta não existe";
      break;
    case LoginErrorCondomino.PASS_WRONG:
      errors.password = "Password errada.";
      break;
  }

  return errors;
}

/* Registo de um condomínio */
async function signupPOST(req: Request, res: Response) {
  /* Dados do pedido */
  const {
    nome,
    nome_admin,
    telemovel_admin,
    email_admin,
    password,
    nif,
    morada,
    cod_postal,
    orcamento_anual,
    dia_pagamento_quota,
  }: {
    nome: string;
    nome_admin: string;
    telemovel_admin: string;
    email_admin: string;
    password: string;
    nif: number;
    morada: string;
    cod_postal: string;
    orcamento_anual: number;
    dia_pagamento_quota: number;
  } = req.body;

  try {
    /* Criar um novo condomínio */
    await Condominio.create({
      nif,
      nome,
      nome_admin,
      email_admin,
      password,
      morada,
      cod_postal,
      telemovel_admin,
      orcamento_anual,
      dia_pagamento_quota,
    });

    /* Enviar resposta para o cliente */
    res.status(200).json({ success: true });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
}

// LOGIN

function loginGET(req: Request, res: Response) {
  res.status(200).json({ success: true });
}

async function loginPOST(req: Request, res: Response) {
  // campos comuns do pedido
  const { password, userType } = req.body;

  // tempo de expiração do cookie
  let cookieAge = 3600000 * 24;

  try {
    if (userType === "condominio") {
      // email do condominio
      const email = req.body.email;
      // fazer o login
      const condominio = await Condominio.login(email, password);
      // criar uma string random para o cookie
      const randString = crypto.randomBytes(128).toString("hex");
      // criar uma hash
      const hash = createHash("sha256");
      // criar uma hash da string random
      const hashedRandString = hash.update(randString, "hex").digest("hex");
      // criar um token de acesso com o id, grupo e a hash da string random
      const accessToken = jwt.sign(
        {
          id: condominio.id,
          id_grupo: condominio.id_grupo,
          hashedRandString,
        },
        process.env.ACCESS_TOKEN_SECRET as string,
        { expiresIn: "1d" },
      );
      // guardar o token na base de dados
      await Condominio.saveToken(condominio.id, accessToken);
      // obter data de expiração do token
      const accDate = jwt.decode(accessToken) as JwtPayload;

      // enviar cookie e token ao cliente
      res
        .status(200)
        .cookie("fp", randString, {
          httpOnly: true,
          maxAge: cookieAge,
        })
        .json({
          id: condominio.id,
          id_grupo: condominio.id_grupo,
          email: condominio.email_admin,
          accessToken: {
            token: accessToken,
            expires: accDate.exp,
          },
        });
      return;
    }

    /**
     * Condómino
     */
    // nome de utilizador do condómino
    const username = req.body.username;
    // fazer o login
    const condomino = await Condomino.login(username, password);
    const randString = crypto.randomBytes(128).toString("hex");
    const hash = createHash("sha256");
    const hashedRandString = hash.update(randString, "hex").digest("hex");
    const accessToken = jwt.sign(
      { id: condomino.id, id_grupo: condomino.id_grupo, hashedRandString },
      process.env.ACCESS_TOKEN_SECRET as string,
      { expiresIn: "1d" },
    );

    // guardar o token na base de dados
    await Condomino.saveToken(condomino.id, accessToken);
    // obter data de expiração do token
    const accDate = jwt.decode(accessToken) as JwtPayload;

    // enviar resposta para o cliente
    res
      .status(200)
      .cookie("fp", randString, {
        httpOnly: true,
        maxAge: cookieAge,
      })
      .json({
        id: condomino.id,
        id_grupo: condomino.id_grupo,
        email: condomino.email_ocupante,
        accessToken: {
          token: accessToken,
          expires: accDate.exp,
        },
      });
  } catch (err) {
    console.log(err);
    const errors = handleLoginErrors(err);
    console.log(errors);
    res.status(400).json({ success: false, errors });
  }
}

const logout: RequestHandler = async (req, res) => {
  const { userType }: { userType: string } = req.body;

  try {
    if (userType === "condomino") {
      await Condomino.update(
        { auth_token: "" },
        {
          where: {
            id: req.condominoID,
            id_condominio: req.condominioID,
          },
        },
      );

      res.status(200).json({ success: true });
      return;
    }

    await Condominio.update(
      { auth_token: "" },
      {
        where: {
          id: req.condominioID,
        },
      },
    );
    res.status(200).json({ success: true });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

export { loginPOST, signupPOST, loginGET, logout };
