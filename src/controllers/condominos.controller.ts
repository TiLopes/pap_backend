import { Request, Response, RequestHandler } from "express";
import { initModels } from "../models/init-models";
import db from "../models";
import { sendCondominoCredentials } from "../utils/sendCondominoEmail";

const { Condomino, Condominio, Fracoes } = initModels(db);

function generatePass() {
  // todos os caracteres possíveis
  const passwordChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#@!%&/";

  // selecionar caracteres aleatoriamente entre 8 e 14
  const password = Array(Math.floor(Math.random() * (14 - 8 + 1)) + 8)
    .fill(passwordChars)
    .map((x) => x[Math.floor(Math.random() * x.length)])
    .join("");

  return password;
}

export const createCondomino: RequestHandler = async (req, res) => {
  // retirar a informação do pedido
  const { fracao, aquisicao, nome, nif, telemovel, email } = req.body;
  // gerar uma password
  const password = generatePass();

  try {
    // criar o condómino
    const condomino = await Condomino.create({
      nome_ocupante: nome,
      nif_ocupante: nif,
      telemovel_ocupante: telemovel,
      email_ocupante: email,
      password: password,
      id_condominio: req.condominioID,
    });

    // gerar o nome de utilizador a partir do id do condominio e do condomino criado anteriormente
    const username = "condomino_" + condomino.id + "_" + condomino.id_condominio;

    // atualizar o condomino com o nome de utilizador
    await Condomino.update({ username }, { where: { id: condomino.id, id_condominio: condomino.id_condominio } });

    for (const f of fracao) {
      // marcar cada uma das frações associadas como ocupadas
      await Fracoes.update(
        { estado: "Ocupado", id_condomino: condomino.id, data_aquisicao: aquisicao },
        { where: { id: f.value, id_condominio: req.condominioID } },
      );
    }

    // enviar os dados de acesso ao condómino
    sendCondominoCredentials(email, password, username);

    res.status(200).json({ success: true });
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false, errors: err });
  }
};

export const getMe: RequestHandler = async (req, res) => {
  try {
    // buscar informações do utilizador
    const meRAW = await Condomino.findOne({
      where: {
        id: req.condominoID,
        id_condominio: req.condominioID,
        banido: false,
      },
      attributes: {
        exclude: ["id", "id_grupo", "password", "auth_token"],
      },
      nest: true,
      include: [
        {
          model: Condominio,
          as: "id_condominio_condominio",
          attributes: ["nome", "morada", "cod_postal"],
        },
      ],
    });

    // formatar o output da query
    let me = {
      nome_ocupante: meRAW?.nome_ocupante,
      nif_ocupante: meRAW?.nif_ocupante,
      telemovel_ocupante: meRAW?.telemovel_ocupante,
      username: meRAW?.username,
      email_ocupante: meRAW?.email_ocupante,
      condominio: {
        nome: meRAW?.id_condominio_condominio.nome,
        morada: meRAW?.id_condominio_condominio.morada,
        cod_postal: meRAW?.id_condominio_condominio.cod_postal,
      },
    };
    res.status(200).json({ success: true, me });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
};

const getCondominos: RequestHandler = async (req, res) => {
  // tipo da busca de informação (básico ou extenso)
  const { type }: { type?: string } = req.params;

  if (Object.keys(req.query).length === 0) {
    // se for básico devolve todos os condóminos (id e nome) do condomínio
    if (type === "basic") return getAllBasic(req, res);
  }

  // parâmetros do pedido
  const { limit, offset, order, dir } = req.query;

  try {
    // procurar e contar o número de condóminos no condomínio
    const condominos = await Condomino.findAndCountAll({
      where: {
        id_condominio: req.condominioID,
        banido: false,
      },
      attributes: {
        exclude: ["password", "auth_token", "id_condominio", "id_grupo"],
      },
      // quantos registos avançar
      offset: parseInt(offset as string, 10),
      // quantos registos serão retornadas
      limit: parseInt(limit as string, 10),
      // ordenar as colunas (nome coluna, direção)
      order: [[order as string, dir as string]],
      nest: true,
    });

    // enviar informação para o front-end
    res.status(200).json({ success: true, condominos: condominos.rows, count: condominos.count });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false, err });
  }
};

async function getAllBasic(req: Request, res: Response) {
  try {
    console.debug("ASFASFFS");
    const condominos = await Condomino.findAll({
      where: {
        id_condominio: req.condominioID,
        banido: false,
      },
      attributes: ["id", "nome_ocupante"],
    });
    console.debug("AAAAA");

    res.status(200).json({ success: true, condominos });
  } catch (err) {
    console.error({ err });
    res.status(400).json({ success: false });
  }
}

// const getAllExtensive: RequestHandler = async (req, res) => {
//   try {
//   } catch (err) {
// console.error({ err });
//     res.status;
//   }
// };

export const banCondomino: RequestHandler = async (req, res) => {
	// id do condómino
  const { id }: { id: number } = req.body;

  try {
		// atualizar estado das frações do condómino para livre
    await Fracoes.update(
      {
        estado: "Livre",
        id_condomino: undefined,
        data_aquisicao: undefined,
      },
      {
        where: {
          id_condominio: req.condominioID,
          id_condomino: id,
        },
      },
    );

		// atualizar estado do condómino para banido
    await Condomino.update(
      {
        banido: true,
      },
      {
        where: {
          id: id,
          id_condominio: req.condominioID,
        },
      },
    );

    res.status(200).json({ success: true });
  } catch (error) {
    console.error(error);
    res.status(400).json({ success: false });
  }
};

export { getCondominos };
