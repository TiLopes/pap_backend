import { Buffer } from "buffer";
import { createWriteStream, PathLike, renameSync } from "fs";
import { dirname, extname } from "path";
import PDFDocument from "pdfkit";
import { Op } from "sequelize";
import { initModels } from "../models/init-models";
import db from "../models";
import { Request, Response } from "express";
import { addMonths } from "date-fns";

const { Condominio, Condomino, Imagens, Ocorrencias } = initModels(db);

const createOcorrenciaGET = async (req: Request, res: Response) => {
  if (req.condominoID) {
    const condomino = await Condomino.findByPk(req.condominoID, {
      attributes: ["nome_ocupante"],
    });
    res.status(200).json({ condomino });
    return;
  }

  const condominio = await Condominio.findByPk(req.condominioID, {
    attributes: ["nome_admin"],
  });

  if (!condominio) {
    res.sendStatus(401);
    return;
  }

  res.status(200).json({ condominio });
};

const createOcorrencia = async (req: Request, res: Response) => {
  const { dataLimite = addMonths(new Date(), 1), dataOcorrencia, descricao, infoAdicional, titulo } = req.body;

  try {
    // contar o número de ocorrências para definir o id por condominio
    const nOcorrencias = await Ocorrencias.count({ where: { id_condominio: req.condominioID } });

    // criar a ocorrência
    const ocorrencia = await Ocorrencias.create({
      id: nOcorrencias + 1,
      id_condominio: req.condominioID,
      // undefined se o autor for o administrador
      autor: req?.condominoID ? req.condominoID : undefined,
      titulo,
      descricao,
      info_adicional: infoAdicional,
      data_ocorrencia: dataOcorrencia,
      data_lim_resolucao: dataLimite,
    });

    // se houver imagens
    if (req.body.files) {
      // para cada imagem associá-la à ocorrência
      req.body.files.forEach(async (f: { name: string; path: PathLike }) => {
        const nome = `OC_${Date.now()}_${req.condominioID}_${ocorrencia.id}${extname(f.name)}`;
        renameSync(f.path, `${dirname(f.path as string)}/${nome}`);

        await Imagens.create({ nome, id_ocorrencia: ocorrencia.id, id_condominio: req.condominioID });
      });
    }

    res.status(200).json({ success: true });
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false });
  }
};

const getOcorrencias = async (req: Request, res: Response) => {
  // parâmetros do pedido
  const { limit, offset, order, dir } = req.query;

  try {
    // procurar e contar o número de frações do condominio
    const ocorrencias = await Ocorrencias.findAndCountAll({
      // procurar por predefinição as ocorrências do condominio ainda por resolver
      where: { id_condominio: req.condominioID, estado: "Pendente" },
      attributes: {
        exclude: ["id_condominio", "autor"],
      },
      include: {
        // incluir a tabela Condómino onde a chave estrangeira da tabela frações
        // é igual à chave primária da tabela condómino
        model: Condomino,
        as: "autor_condomino",
        attributes: ["nome_ocupante"],
      },
      // quantos registos avançar
      offset: parseInt(offset as string, 10),
      // quantos registos serão retornadas
      limit: parseInt(limit as string, 10),
      // ordenar as colunas (nome coluna, direção)
      order: [[order as string, dir as string]],
      raw: true,
      nest: true,
    });

    res.status(200).json({ success: true, ocorrencias: ocorrencias.rows, count: ocorrencias.count });
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false });
  }
};

const getOcorrencia = async (req: Request, res: Response) => {
  const { id } = req.params; // id da ocorrência a procurar

  try {
    const ocorrencia = await Ocorrencias.findByPk(id, {
      attributes: {
        exclude: ["autor"],
      },
      include: {
        // incluir informações do condómino
        model: Condomino,
        as: "autor_condomino",
        attributes: ["nome_ocupante"],
      },
      nest: true,
    });

    // procurar todas as imagens associadas à ocorrência
    const ocorrenciaImagens = await Imagens.findAll({
      where: {
        id_ocorrencia: ocorrencia?.id,
        id_condominio: req.condominioID,
      },
      attributes: ["nome"],
    });

    res.status(200).json({ success: true, ocorrencia, images: ocorrenciaImagens });
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false });
  }
};

const solveOcorrencia = async (req: Request, res: Response) => {
  // id da ocorrencia
  const { id } = req.params;

  try {
    // atualizar estado da ocorrência para "Resolvida"
    await Ocorrencias.update(
      { estado: "Resolvida" },
      {
        where: { id, id_condominio: req.condominioID },
      },
    );
    res.status(200).json({ success: true });
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false });
  }
};

const filterOcorrencias = async (req: Request, res: Response) => {
  // retirar os filtros dos parâmetros
  const { data_inicial, data_final, estado, limit, offset, order, dir } = req.query;

  const where = {};
  where[Op.and] = [];

  // adicionar condição se o parâmetro existir no pedido
  if (data_inicial) where[Op.and].push({ data_ocorrencia: { [Op.gte]: data_inicial } });

  // adicionar condição se o parâmetro existir no pedido
  if (data_final) where[Op.and].push({ data_ocorrencia: { [Op.lte]: data_final } });

  // adicionar condição se o parâmetro existir no pedido
  if (estado) where[Op.and].push({ estado: { [Op.eq]: estado } });

  try {
    const ocorrencias = await Ocorrencias.findAndCountAll({
      where,
      // quantos registos avançar
      offset: parseInt(offset as string, 10),
      // quantos registos serão retornadas
      limit: parseInt(limit as string, 10),
      // ordenar as colunas (nome coluna, direção)
      order: [[order as string, dir as string]],
      include: {
        model: Condomino,
        as: "autor_condomino",
        attributes: ["nome_ocupante"],
      },
      raw: true,
      nest: true,
    });

    res.status(200).json({ success: true, ocorrencias: ocorrencias.rows, count: ocorrencias.count });
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false });
  }
};

const imprimirOcorrencias = async (req: Request, res: Response) => {
  const { data_de, data_ate } = req.query; // período

  try {
    // informação do condomínio
    const condominio = await Condominio.findByPk(req.condominioID);
    // procurar todas as ocorrências dentro do período
    const ocorrencias = await Ocorrencias.findAll({
      where: {
        data_ocorrencia: {
          [Op.gte]: data_de as string,
          [Op.lte]: data_ate as string,
        },
      },
      attributes: { exclude: ["autor"] },
      include: {
        model: Condomino,
        attributes: ["nome_ocupante"],
        as: "autor_condomino",
      },
      order: [["estado", "DESC"]],
      nest: true,
    });

    // criar um novo documento PDF
    const doc = new PDFDocument({
      size: "A4",
      font: "Helvetica",
      bufferPages: true,
      compress: true,
    });
    const buffers: any[] = [];

    doc.on("data", buffers.push.bind(buffers));

    // quando terminar de gerar o PDF enviar para o front-end
    doc.on("end", () => {
      const pdfData = Buffer.concat(buffers);
      res
        .writeHead(200, {
          "Content-Length": Buffer.byteLength(pdfData),
          "Content-Type": "application/pdf",
          "Content-disposition": "attachment;filename=ocorrencias.pdf",
        })
        .end(pdfData);
    });

    // caminho do ficheiro PDF
    doc.pipe(createWriteStream(`${global.uploadDir}/output.pdf`));
    // para cada ocorrência adicionar a informação no PDF
    ocorrencias.forEach(async (ocorrencia, i) => {
      doc.font("Helvetica").fontSize(11).text("Registo de ocorrência");
      doc.moveDown(0.75);
      doc.font("Helvetica-Bold").fontSize(11).text(`Nº registo: `, {
        continued: true,
      });
      doc.font("Helvetica").fontSize(11).text(ocorrencia.id.toString());
      doc.font("Helvetica-Bold").fontSize(11).text(`NIF do condomínio: `, {
        continued: true,
      });
      doc
        .font("Helvetica")
        .fontSize(11)
        .text(condominio?.nif.toString() as string);
      doc.font("Helvetica-Bold").fontSize(11).text(`Data registo: `, {
        continued: true,
      });
      doc.font("Helvetica").fontSize(11).text(ocorrencia.data_ocorrencia);
      doc.font("Helvetica-Bold").fontSize(11).text(`Registo por: `, {
        continued: true,
      });
      doc
        .font("Helvetica")
        .fontSize(11)
        .text(ocorrencia?.autor_condomino?.nome_ocupante || "Administrador");
      doc.moveDown(0.5);
      doc.font("Helvetica-Bold").fontSize(11).text(`Título: `, {
        continued: true,
      });
      doc.font("Helvetica").fontSize(11).text(ocorrencia.titulo);
      doc.font("Helvetica-Bold").fontSize(11).text(`Descritivo:`);
      doc
        .font("Helvetica")
        .fontSize(11)
        .text(`${ocorrencia.descricao.replace(/(\r\n|\n|\r)/gm, "\n")}`);
      doc.font("Helvetica-Bold").fontSize(11).text("Informação adicional:");
      doc
        .font("Helvetica")
        .fontSize(11)
        .text(`${ocorrencia.info_adicional?.replace(/(\r\n|\n|\r)/gm, "\n")}`);
      doc.font("Helvetica-Bold").fontSize(11).text(`Estado: `, {
        continued: true,
      });
      doc.font("Helvetica").fontSize(11).text(ocorrencia.estado);
      doc.moveDown(1.5);
      if (i !== ocorrencias.length - 1) {
        doc
          .moveTo(doc.x + 50, doc.y)
          .lineTo(doc.page.width - doc.x - 50, doc.y)
          .stroke();
      }
      doc.moveDown(1.5);
    });

    // Adicionar cabeçalho e rodapé
    const pages = doc.bufferedPageRange();
    for (let i = 0; i < pages.count; i++) {
      doc.switchToPage(i);

      // Adicionar número da página no cabeçalho
      const oldTopMargin = doc.page.margins.top;
      doc.page.margins.top = 0;
      doc
        .font("Helvetica-Bold")
        .fontSize(11)
        .text(`Condomínio - ${condominio!.nome}`, doc.x, oldTopMargin / 4, {
          align: "right",
        });
      doc
        .font("Helvetica-Bold")
        .fontSize(11)
        .text(condominio!.morada as string, doc.x, oldTopMargin / 2, {
          align: "right",
        });

      doc.page.margins.top = oldTopMargin;

      // Adicionar número da página no rodapé
      const oldBottomMargin = doc.page.margins.bottom;
      doc.page.margins.bottom = 0;
      doc
        .font("Helvetica")
        .fontSize(10)
        .text(`Página ${i + 1}/${pages.count}`, doc.x, doc.page.height - oldBottomMargin / 2, { align: "right" });
      doc.page.margins.bottom = oldBottomMargin;
    }
    doc.end();
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false });
  }
};

export {
  getOcorrencia,
  getOcorrencias,
  createOcorrencia,
  createOcorrenciaGET,
  solveOcorrencia,
  filterOcorrencias,
  imprimirOcorrencias,
};
