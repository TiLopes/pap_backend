import config from "../config";
import { createTransport } from "nodemailer";

export function sendCondominoCredentials(condomino: string, password: string, username: string) {
  const transporter = createTransport({
    host: config.SMTP_SERVER, // host do servidor SMTP
    port: config.SMTP_PORT, // porta do servidor SMTP
    auth: {
      // autenticação no servidor
      user: config.EMAIL_SENDER,
      pass: config.EMAIL_PASS,
    },
  });

  const mailOptions = {
    from: config.EMAIL_SENDER, // email do projeto
    to: condomino, // email do condómino
    subject: "Acesso à nossa plataforma", // assunto
    // conteúdo do email em html
    html: `<!doctype html>
    <html>
      <body>
        <h1>Foi associado a um condomínio na nossa plataforma!</h1>
        <h2>Utilize estes dados de acesso para aceder:</h2>
			<ul>
        <li>Nome de utilizador: ${username}</li>
        <li>Password: ${password}</li>
			</ul>
			<p>Pode utilizar a seguinte hiperligação para entrar na plataforma: <a href="${config.API_SERVER}/login">${config.API_SERVER}/login<a/>
		</p>
      </body>
    </html>`,
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
    }
  });
}
