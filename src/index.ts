// importar a biblioteca
import express from "express";
import dotenv from "dotenv";
import cors from "cors";
import cookieParser from "cookie-parser";
import formData from "express-form-data";
import path from "path";
import sanitizer from "perfect-express-sanitizer";
import authRouter from "./routes/auth.routes";
import protectedRouter from "./routes/protected.routes";
import quotasRoutes from "./routes/quotas.routes";
import movimentosRoutes from "./routes/movimentos.routes";
import condominosRoutes from "./routes/condominos.routes";
import fracoesRoutes from "./routes/fracoes.routes";
import ocorrenciasRoutes from "./routes/ocorrencias.routes";
import condominioRoutes from "./routes/condominio.routes";
import { CronJob } from "cron";
import { checkAtrasoQuota } from "./cron/checkAtrasoQuota";

dotenv.config();

// criar uma instância do servidor
const app = express();

// definir uma porta
const PORT = 3000 || process.env.PORT;

const uploadDir: string = path.join(__dirname + "/uploads");

global.uploadDir = uploadDir;

app.use(
  cors({
    origin: ["http://localhost:5173", "http://localhost"],
    credentials: true,
  }),
);
app.use(cookieParser());
app.use(express.json());
app.use("/static", express.static(uploadDir));
app.use(
  formData.parse({
    uploadDir: uploadDir,
    autoClean: false,
  }),
);
app.use(formData.format());
app.use(formData.union());

app.use(
  sanitizer.clean(
    {
      xss: true,
      noSql: true,
      sql: true,
    }["/create/ocorrencia"],
  ),
);

app.get("/", (_, res) => {
  res.sendStatus(200);
});
app.use("/", authRouter);
app.use("/", protectedRouter);
app.use("/api", quotasRoutes);
app.use("/api", movimentosRoutes);
app.use("/api", condominosRoutes);
app.use("/api", fracoesRoutes);
app.use("/api", ocorrenciasRoutes);
app.use("/api", condominioRoutes);

// iniciar o servidor
app.listen(PORT, () => console.info(`API na porta: ${PORT}`));

// novo serviço executado no primeiro minuto do dia
const job = new CronJob(
  "0 1 * * *",
  () => {
    checkAtrasoQuota();
  },
  null,
  true,
);
