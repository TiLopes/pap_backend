import { NextFunction, Request, Response } from "express";
import db from "../models";
import { initModels } from "../models/init-models";
const { Condominio, Condomino, GroupPermissions } = initModels(db);

/**
 * Middleware que verifica as permissões do utilizador
 */
async function verifyPermission(req: Request, res: Response, next: NextFunction) {
  /* Obter o token do pedido que se encontra no header "Authorization" */
  const token = req.headers.authorization?.split(" ")[1];
  /* Variável que vai guardar o grupo do utilizador */
  let groupID: number;

  /* Saber se é um condómino */
  const condominio = await Condominio.findOne({
    where: { auth_token: token },
  });

  if (condominio) {
    groupID = condominio.id_grupo;
  } else {
    /* Saber se é um condomínio */
    const condomino = await Condomino.findOne({ where: { auth_token: token } });

    /* Se não existir em nenhuma das tabelas é inválido */
    if (!condomino) {
      res.status(403).json({ success: false, error: "TOKEN_INVALID" });
      return;
    }

    groupID = condomino.id_grupo;
  }

  /* Procurar todas as permissões do grupo do utilizador */
  const gps = await GroupPermissions.findAll({
    where: { group_id: groupID },
    attributes: ["permission_id"],
  });

  /* Array com todas as permissões do utilizador */
  let acl: string[] = [];
  for (const perm of gps) {
    acl.push(perm.permission_id);
  }

  /* Obter o URL da rota do pedido */
  const url = new URL(req.protocol + "://" + req.get("host") + req.originalUrl);

  /* Dividir nas barras "/" */
  const pathSplit = url.pathname.split("/");
  /* Retirar o terceiro elemento depois da rota estar separada */
  const action = pathSplit[2];
  /* Retirar o quarto elemento depois da rota estar separada */
  const spec = pathSplit[3];
  /* Formar a syntax de uma permissão */
  const fullPerm = action + ":" + spec;

	/* Ver se a permissão existe no array acl */
  if (acl.includes(fullPerm.toLowerCase()) || acl.includes("*:*")) {
    return next();
  }

	/* Permissão não existe e é enviado um erro */
  res.status(401).json({ error: "PERM_INVALID" });
}

export default verifyPermission;
