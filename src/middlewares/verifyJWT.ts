import { NextFunction, Request, Response } from "express";
import jwt, { VerifyErrors } from "jsonwebtoken";
import { createHash } from "node:crypto";
import db from "../models";
import { initModels } from "../models/init-models";
const { Condominio, Condomino } = initModels(db);

/**
 * Middleware que verifica os JsonWebTokens
 */
async function verifyJWT(req: Request, res: Response, next: NextFunction) {
  /**
   * Obter o token do pedido que se encontra no header "Authorization"
   */
  const token: string | undefined = req.headers.authorization?.split(" ")[1];
  /**
   * Obter os cookies
   */
  const cookies = req.cookies;
  /**
   * Apenas buscar o token criado pela API
   */
  const fp = cookies.fp;

  /**
   * Se não houver token proibir o utilizador de aceder à rota
   */
  if (!token) {
    res.status(401).json({ success: false, error: "TOKEN_EMPTY" });
    return;
  }

  /**
   * Se não houver o cookie da API proibir o utilizador de aceder à rota
   */
  if (!fp) {
    res.status(401).json({ success: false, error: "TOKEN_INVALID" });
    return;
  }

  /* Verificar se o token é válido */
  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET as string,
		async (err: VerifyErrors | null, decodedToken: any) => {
    /* Se for inválido proibir o utilizador de aceder à rota */
    if (err) {
      console.error(err);
      res.status(403).json({ success: false, error: "TOKEN_INVALID" });
      return;
    }

    const hash = createHash("sha256");
    const finalString = hash.update(fp, "hex").digest("hex");

    /* Se for inválido proibir o utilizador de aceder à rota */
    if (finalString !== decodedToken.hashedRandString) {
      console.info({ finalString, hashed: decodedToken.hashedRandString });
      res.status(403).json({ success: false, error: "TOKEN_INVALID" });
      return;
    }

    /* Saber se é um condómino */
    const condomino = await Condomino.findOne({
      where: { auth_token: token },
    });

    if (condomino) {
      /* Adicionar parâmetros ao pedido para fazer a pesquisa na base de dados */
      req.condominoID = condomino.id;
      req.condominioID = condomino.id_condominio;
      return next();
    }

    /* Saber se é um condomínio */
    const condominio = await Condominio.findOne({
      where: { auth_token: token },
    });

    if (condominio) {
      /* Adicionar parâmetros ao pedido para fazer a pesquisa na base de dados */
      req.condominioID = condominio.id;
      return next();
    }

    /* Se não existir em nenhuma das tabelas é inválido */
    res.status(403).json({ success: false, error: "TOKEN_INVALID" });
    return;
  });
}

export default verifyJWT;
