const config = {
  development: {
    EMAIL_SENDER: "",
    EMAIL_PASS: "",
    SMTP_SERVER: "",
    SMTP_PORT: 0,
    API_SERVER: "",
  },
  production: {
    EMAIL_SENDER: "",
    EMAIL_PASS: "",
    SMTP_SERVER: "",
    SMTP_PORT: 0,
    API_SERVER: "",
  },
};

export default config[process.env.NODE_ENV || "development"];
