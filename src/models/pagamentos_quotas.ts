import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Condomino, CondominoId } from './condomino';
import type { Quota, QuotaId } from './quota';

export interface PagamentosQuotasAttributes {
  id: number;
  data: string;
  observacoes?: string;
  id_condomino?: number;
}

export type PagamentosQuotasPk = "id";
export type PagamentosQuotasId = PagamentosQuotas[PagamentosQuotasPk];
export type PagamentosQuotasOptionalAttributes = "observacoes" | "id_condomino";
export type PagamentosQuotasCreationAttributes = Optional<PagamentosQuotasAttributes, PagamentosQuotasOptionalAttributes>;

export class PagamentosQuotas extends Model<PagamentosQuotasAttributes, PagamentosQuotasCreationAttributes> implements PagamentosQuotasAttributes {
  id!: number;
  data!: string;
  observacoes?: string;
  id_condomino?: number;

  // PagamentosQuotas belongsTo Condomino via id_condomino
  id_condomino_condomino!: Condomino;
  getId_condomino_condomino!: Sequelize.BelongsToGetAssociationMixin<Condomino>;
  setId_condomino_condomino!: Sequelize.BelongsToSetAssociationMixin<Condomino, CondominoId>;
  createId_condomino_condomino!: Sequelize.BelongsToCreateAssociationMixin<Condomino>;
  // PagamentosQuotas hasMany Quota via id_pagamento
  quota!: Quota[];
  getQuota!: Sequelize.HasManyGetAssociationsMixin<Quota>;
  setQuota!: Sequelize.HasManySetAssociationsMixin<Quota, QuotaId>;
  addQuotum!: Sequelize.HasManyAddAssociationMixin<Quota, QuotaId>;
  addQuota!: Sequelize.HasManyAddAssociationsMixin<Quota, QuotaId>;
  createQuotum!: Sequelize.HasManyCreateAssociationMixin<Quota>;
  removeQuotum!: Sequelize.HasManyRemoveAssociationMixin<Quota, QuotaId>;
  removeQuota!: Sequelize.HasManyRemoveAssociationsMixin<Quota, QuotaId>;
  hasQuotum!: Sequelize.HasManyHasAssociationMixin<Quota, QuotaId>;
  hasQuota!: Sequelize.HasManyHasAssociationsMixin<Quota, QuotaId>;
  countQuota!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof PagamentosQuotas {
    return PagamentosQuotas.init({
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    data: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    observacoes: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    id_condomino: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'condomino',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'pagamentos_quotas',
    schema: 'papv2',
    timestamps: false,
    indexes: [
      {
        name: "pagamentos_quotas_pk",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  }
}
