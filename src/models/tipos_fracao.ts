import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Fracoes, FracoesId } from './fracoes';

export interface TiposFracaoAttributes {
  id: number;
  descricao: string;
}

export type TiposFracaoPk = "id";
export type TiposFracaoId = TiposFracao[TiposFracaoPk];
export type TiposFracaoOptionalAttributes = "id";
export type TiposFracaoCreationAttributes = Optional<TiposFracaoAttributes, TiposFracaoOptionalAttributes>;

export class TiposFracao extends Model<TiposFracaoAttributes, TiposFracaoCreationAttributes> implements TiposFracaoAttributes {
  id!: number;
  descricao!: string;

  // TiposFracao hasMany Fracoes via tipo
  fracos!: Fracoes[];
  getFracos!: Sequelize.HasManyGetAssociationsMixin<Fracoes>;
  setFracos!: Sequelize.HasManySetAssociationsMixin<Fracoes, FracoesId>;
  addFraco!: Sequelize.HasManyAddAssociationMixin<Fracoes, FracoesId>;
  addFracos!: Sequelize.HasManyAddAssociationsMixin<Fracoes, FracoesId>;
  createFraco!: Sequelize.HasManyCreateAssociationMixin<Fracoes>;
  removeFraco!: Sequelize.HasManyRemoveAssociationMixin<Fracoes, FracoesId>;
  removeFracos!: Sequelize.HasManyRemoveAssociationsMixin<Fracoes, FracoesId>;
  hasFraco!: Sequelize.HasManyHasAssociationMixin<Fracoes, FracoesId>;
  hasFracos!: Sequelize.HasManyHasAssociationsMixin<Fracoes, FracoesId>;
  countFracos!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof TiposFracao {
    return TiposFracao.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    descricao: {
      type: DataTypes.STRING(30),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tipos_fracao',
    schema: 'papv2',
    timestamps: false,
    indexes: [
      {
        name: "tipos_fracao_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  }
}
