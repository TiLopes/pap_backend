import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Condominio, CondominioId } from './condominio';
import type { Condomino, CondominoId } from './condomino';

export interface MovimentosAttributes {
  id: number;
  tipo: string;
  observacoes?: string;
  id_condominio: number;
  data: string;
  valor: number;
  descritivo: string;
  id_condomino?: number;
}

export type MovimentosPk = "id" | "id_condominio";
export type MovimentosId = Movimentos[MovimentosPk];
export type MovimentosOptionalAttributes = "observacoes" | "id_condomino";
export type MovimentosCreationAttributes = Optional<MovimentosAttributes, MovimentosOptionalAttributes>;

export class Movimentos extends Model<MovimentosAttributes, MovimentosCreationAttributes> implements MovimentosAttributes {
  id!: number;
  tipo!: string;
  observacoes?: string;
  id_condominio!: number;
  data!: string;
  valor!: number;
  descritivo!: string;
  id_condomino?: number;

  // Movimentos belongsTo Condominio via id_condominio
  id_condominio_condominio!: Condominio;
  getId_condominio_condominio!: Sequelize.BelongsToGetAssociationMixin<Condominio>;
  setId_condominio_condominio!: Sequelize.BelongsToSetAssociationMixin<Condominio, CondominioId>;
  createId_condominio_condominio!: Sequelize.BelongsToCreateAssociationMixin<Condominio>;
  // Movimentos belongsTo Condomino via id_condomino
  id_condomino_condomino!: Condomino;
  getId_condomino_condomino!: Sequelize.BelongsToGetAssociationMixin<Condomino>;
  setId_condomino_condomino!: Sequelize.BelongsToSetAssociationMixin<Condomino, CondominoId>;
  createId_condomino_condomino!: Sequelize.BelongsToCreateAssociationMixin<Condomino>;

	// definição da estrutura da tabela (tipo da coluna, é opcional, é única, é auto-incrementavel, etc.)
  static initModel(sequelize: Sequelize.Sequelize): typeof Movimentos {
    return Movimentos.init({
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    tipo: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    observacoes: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    id_condominio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'condominio',
        key: 'id'
      }
    },
    data: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    valor: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    descritivo: {
      type: DataTypes.STRING(60),
      allowNull: false
    },
    id_condomino: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'condomino',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'movimentos',
    schema: 'papv2',
    timestamps: false,
    indexes: [
      {
        name: "fki_i",
        fields: [
          { name: "id_condominio" },
        ]
      },
      {
        name: "movimentos_pkey",
        unique: true,
        fields: [
          { name: "id" },
          { name: "id_condominio" },
        ]
      },
    ]
  });
  }
}
