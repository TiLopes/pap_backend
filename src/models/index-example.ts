import { Sequelize } from "sequelize";

/**
 * Criar instância da base de dados
 */
const db: Sequelize = new Sequelize({
  username: "",
  password: "",
  database: "",
  schema: "",
  host: "",
  dialect: "",
  define: {
    timestamps: false,
  },
});

export default db;
