import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Ocorrencias, OcorrenciasId } from './ocorrencias';

export interface ImagensAttributes {
  id: number;
  nome: string;
  id_ocorrencia: number;
  id_condominio?: number;
}

export type ImagensPk = "id";
export type ImagensId = Imagens[ImagensPk];
export type ImagensOptionalAttributes = "id" | "id_condominio";
export type ImagensCreationAttributes = Optional<ImagensAttributes, ImagensOptionalAttributes>;

export class Imagens extends Model<ImagensAttributes, ImagensCreationAttributes> implements ImagensAttributes {
  id!: number;
  nome!: string;
  id_ocorrencia!: number;
  id_condominio?: number;

  // Imagens belongsTo Ocorrencias via id_condominio
  id_condominio_ocorrencia!: Ocorrencias;
  getId_condominio_ocorrencia!: Sequelize.BelongsToGetAssociationMixin<Ocorrencias>;
  setId_condominio_ocorrencia!: Sequelize.BelongsToSetAssociationMixin<Ocorrencias, OcorrenciasId>;
  createId_condominio_ocorrencia!: Sequelize.BelongsToCreateAssociationMixin<Ocorrencias>;
  // Imagens belongsTo Ocorrencias via id_ocorrencia
  id_ocorrencia_ocorrencia!: Ocorrencias;
  getId_ocorrencia_ocorrencia!: Sequelize.BelongsToGetAssociationMixin<Ocorrencias>;
  setId_ocorrencia_ocorrencia!: Sequelize.BelongsToSetAssociationMixin<Ocorrencias, OcorrenciasId>;
  createId_ocorrencia_ocorrencia!: Sequelize.BelongsToCreateAssociationMixin<Ocorrencias>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Imagens {
    return Imagens.init({
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nome: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    id_ocorrencia: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'ocorrencias',
        key: 'id_condominio'
      }
    },
    id_condominio: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'ocorrencias',
        key: 'id_condominio'
      }
    }
  }, {
    sequelize,
    tableName: 'imagens',
    schema: 'papv2',
    timestamps: false,
    indexes: [
      {
        name: "idx_16609_imagens_un",
        unique: true,
        fields: [
          { name: "nome" },
        ]
      },
      {
        name: "idx_16609_primary",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  }
}
