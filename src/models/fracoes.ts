import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Condominio, CondominioId } from './condominio';
import type { Condomino, CondominoId } from './condomino';
import type { Quota, QuotaId } from './quota';
import type { TiposFracao, TiposFracaoId } from './tipos_fracao';

export interface FracoesAttributes {
  id: string;
  andar?: string;
  permilagem_escritura: number;
  estado: string;
  id_condominio: number;
  id_condomino?: number;
  permilagem_rateio: number;
  data_aquisicao?: string;
  tipo?: number;
}

export type FracoesPk = "id" | "id_condominio";
export type FracoesId = Fracoes[FracoesPk];
export type FracoesOptionalAttributes = "andar" | "permilagem_escritura" | "estado" | "id_condomino" | "permilagem_rateio" | "data_aquisicao" | "tipo";
export type FracoesCreationAttributes = Optional<FracoesAttributes, FracoesOptionalAttributes>;

export class Fracoes extends Model<FracoesAttributes, FracoesCreationAttributes> implements FracoesAttributes {
  id!: string;
  andar?: string;
  permilagem_escritura!: number;
  estado!: string;
  id_condominio!: number;
  id_condomino?: number;
  permilagem_rateio!: number;
  data_aquisicao?: string;
  tipo?: number;

  // Fracoes belongsTo Condominio via id_condominio
  id_condominio_condominio!: Condominio;
  getId_condominio_condominio!: Sequelize.BelongsToGetAssociationMixin<Condominio>;
  setId_condominio_condominio!: Sequelize.BelongsToSetAssociationMixin<Condominio, CondominioId>;
  createId_condominio_condominio!: Sequelize.BelongsToCreateAssociationMixin<Condominio>;
  // Fracoes belongsTo Condomino via id_condomino
  id_condomino_condomino!: Condomino;
  getId_condomino_condomino!: Sequelize.BelongsToGetAssociationMixin<Condomino>;
  setId_condomino_condomino!: Sequelize.BelongsToSetAssociationMixin<Condomino, CondominoId>;
  createId_condomino_condomino!: Sequelize.BelongsToCreateAssociationMixin<Condomino>;
  // Fracoes hasMany Quota via id_condominio
  quota!: Quota[];
  getQuota!: Sequelize.HasManyGetAssociationsMixin<Quota>;
  setQuota!: Sequelize.HasManySetAssociationsMixin<Quota, QuotaId>;
  addQuotum!: Sequelize.HasManyAddAssociationMixin<Quota, QuotaId>;
  addQuota!: Sequelize.HasManyAddAssociationsMixin<Quota, QuotaId>;
  createQuotum!: Sequelize.HasManyCreateAssociationMixin<Quota>;
  removeQuotum!: Sequelize.HasManyRemoveAssociationMixin<Quota, QuotaId>;
  removeQuota!: Sequelize.HasManyRemoveAssociationsMixin<Quota, QuotaId>;
  hasQuotum!: Sequelize.HasManyHasAssociationMixin<Quota, QuotaId>;
  hasQuota!: Sequelize.HasManyHasAssociationsMixin<Quota, QuotaId>;
  countQuota!: Sequelize.HasManyCountAssociationsMixin;
  // Fracoes hasMany Quota via id_fracao
  id_fracao_quota!: Quota[];
  getId_fracao_quota!: Sequelize.HasManyGetAssociationsMixin<Quota>;
  setId_fracao_quota!: Sequelize.HasManySetAssociationsMixin<Quota, QuotaId>;
  addId_fracao_quotum!: Sequelize.HasManyAddAssociationMixin<Quota, QuotaId>;
  addId_fracao_quota!: Sequelize.HasManyAddAssociationsMixin<Quota, QuotaId>;
  createId_fracao_quotum!: Sequelize.HasManyCreateAssociationMixin<Quota>;
  removeId_fracao_quotum!: Sequelize.HasManyRemoveAssociationMixin<Quota, QuotaId>;
  removeId_fracao_quota!: Sequelize.HasManyRemoveAssociationsMixin<Quota, QuotaId>;
  hasId_fracao_quotum!: Sequelize.HasManyHasAssociationMixin<Quota, QuotaId>;
  hasId_fracao_quota!: Sequelize.HasManyHasAssociationsMixin<Quota, QuotaId>;
  countId_fracao_quota!: Sequelize.HasManyCountAssociationsMixin;
  // Fracoes belongsTo TiposFracao via tipo
  tipo_tipos_fracao!: TiposFracao;
  getTipo_tipos_fracao!: Sequelize.BelongsToGetAssociationMixin<TiposFracao>;
  setTipo_tipos_fracao!: Sequelize.BelongsToSetAssociationMixin<TiposFracao, TiposFracaoId>;
  createTipo_tipos_fracao!: Sequelize.BelongsToCreateAssociationMixin<TiposFracao>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Fracoes {
    return Fracoes.init({
    id: {
      type: DataTypes.STRING(5),
      allowNull: false,
      primaryKey: true
    },
    andar: {
      type: DataTypes.STRING(20),
      allowNull: true,
      defaultValue: "NULL"
    },
    permilagem_escritura: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: 0.00
    },
    estado: {
      type: DataTypes.STRING(50),
      allowNull: false,
      defaultValue: "Livre"
    },
    id_condominio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'condominio',
        key: 'id'
      }
    },
    id_condomino: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'condomino',
        key: 'id'
      }
    },
    permilagem_rateio: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: 0.00
    },
    data_aquisicao: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    tipo: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'tipos_fracao',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'fracoes',
    schema: 'papv2',
    timestamps: false,
    indexes: [
      {
        name: "idx_16597_fracoes_fk",
        fields: [
          { name: "id_condominio" },
        ]
      },
      {
        name: "idx_16597_fracoes_fk_1",
        fields: [
          { name: "id_condomino" },
        ]
      },
      {
        name: "idx_16597_primary",
        unique: true,
        fields: [
          { name: "id" },
          { name: "id_condominio" },
        ]
      },
    ]
  });
  }
}
