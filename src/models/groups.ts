import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Condominio, CondominioId } from './condominio';
import type { Condomino, CondominoId } from './condomino';
import type { GroupPermissions, GroupPermissionsId } from './group_permissions';
import type { Permissions, PermissionsId } from './permissions';

export interface GroupsAttributes {
  id: number;
  nome: string;
}

export type GroupsPk = "id";
export type GroupsId = Groups[GroupsPk];
export type GroupsCreationAttributes = GroupsAttributes;

export class Groups extends Model<GroupsAttributes, GroupsCreationAttributes> implements GroupsAttributes {
  id!: number;
  nome!: string;

  // Groups hasMany Condominio via id_grupo
  condominios!: Condominio[];
  getCondominios!: Sequelize.HasManyGetAssociationsMixin<Condominio>;
  setCondominios!: Sequelize.HasManySetAssociationsMixin<Condominio, CondominioId>;
  addCondominio!: Sequelize.HasManyAddAssociationMixin<Condominio, CondominioId>;
  addCondominios!: Sequelize.HasManyAddAssociationsMixin<Condominio, CondominioId>;
  createCondominio!: Sequelize.HasManyCreateAssociationMixin<Condominio>;
  removeCondominio!: Sequelize.HasManyRemoveAssociationMixin<Condominio, CondominioId>;
  removeCondominios!: Sequelize.HasManyRemoveAssociationsMixin<Condominio, CondominioId>;
  hasCondominio!: Sequelize.HasManyHasAssociationMixin<Condominio, CondominioId>;
  hasCondominios!: Sequelize.HasManyHasAssociationsMixin<Condominio, CondominioId>;
  countCondominios!: Sequelize.HasManyCountAssociationsMixin;
  // Groups hasMany Condomino via id_grupo
  condominos!: Condomino[];
  getCondominos!: Sequelize.HasManyGetAssociationsMixin<Condomino>;
  setCondominos!: Sequelize.HasManySetAssociationsMixin<Condomino, CondominoId>;
  addCondomino!: Sequelize.HasManyAddAssociationMixin<Condomino, CondominoId>;
  addCondominos!: Sequelize.HasManyAddAssociationsMixin<Condomino, CondominoId>;
  createCondomino!: Sequelize.HasManyCreateAssociationMixin<Condomino>;
  removeCondomino!: Sequelize.HasManyRemoveAssociationMixin<Condomino, CondominoId>;
  removeCondominos!: Sequelize.HasManyRemoveAssociationsMixin<Condomino, CondominoId>;
  hasCondomino!: Sequelize.HasManyHasAssociationMixin<Condomino, CondominoId>;
  hasCondominos!: Sequelize.HasManyHasAssociationsMixin<Condomino, CondominoId>;
  countCondominos!: Sequelize.HasManyCountAssociationsMixin;
  // Groups hasMany GroupPermissions via group_id
  group_permissions!: GroupPermissions[];
  getGroup_permissions!: Sequelize.HasManyGetAssociationsMixin<GroupPermissions>;
  setGroup_permissions!: Sequelize.HasManySetAssociationsMixin<GroupPermissions, GroupPermissionsId>;
  addGroup_permission!: Sequelize.HasManyAddAssociationMixin<GroupPermissions, GroupPermissionsId>;
  addGroup_permissions!: Sequelize.HasManyAddAssociationsMixin<GroupPermissions, GroupPermissionsId>;
  createGroup_permission!: Sequelize.HasManyCreateAssociationMixin<GroupPermissions>;
  removeGroup_permission!: Sequelize.HasManyRemoveAssociationMixin<GroupPermissions, GroupPermissionsId>;
  removeGroup_permissions!: Sequelize.HasManyRemoveAssociationsMixin<GroupPermissions, GroupPermissionsId>;
  hasGroup_permission!: Sequelize.HasManyHasAssociationMixin<GroupPermissions, GroupPermissionsId>;
  hasGroup_permissions!: Sequelize.HasManyHasAssociationsMixin<GroupPermissions, GroupPermissionsId>;
  countGroup_permissions!: Sequelize.HasManyCountAssociationsMixin;
  // Groups belongsToMany Permissions via group_id and permission_id
  permission_id_permissions!: Permissions[];
  getPermission_id_permissions!: Sequelize.BelongsToManyGetAssociationsMixin<Permissions>;
  setPermission_id_permissions!: Sequelize.BelongsToManySetAssociationsMixin<Permissions, PermissionsId>;
  addPermission_id_permission!: Sequelize.BelongsToManyAddAssociationMixin<Permissions, PermissionsId>;
  addPermission_id_permissions!: Sequelize.BelongsToManyAddAssociationsMixin<Permissions, PermissionsId>;
  createPermission_id_permission!: Sequelize.BelongsToManyCreateAssociationMixin<Permissions>;
  removePermission_id_permission!: Sequelize.BelongsToManyRemoveAssociationMixin<Permissions, PermissionsId>;
  removePermission_id_permissions!: Sequelize.BelongsToManyRemoveAssociationsMixin<Permissions, PermissionsId>;
  hasPermission_id_permission!: Sequelize.BelongsToManyHasAssociationMixin<Permissions, PermissionsId>;
  hasPermission_id_permissions!: Sequelize.BelongsToManyHasAssociationsMixin<Permissions, PermissionsId>;
  countPermission_id_permissions!: Sequelize.BelongsToManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Groups {
    return Groups.init({
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    nome: {
      type: DataTypes.STRING(45),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'groups',
    schema: 'papv2',
    timestamps: false,
    indexes: [
      {
        name: "idx_16602_name_unique",
        unique: true,
        fields: [
          { name: "nome" },
        ]
      },
      {
        name: "idx_16602_primary",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  }
}
