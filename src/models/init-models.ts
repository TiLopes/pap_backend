import type { Sequelize } from "sequelize";
import { Condominio as _Condominio } from "./condominio";
import type { CondominioAttributes, CondominioCreationAttributes } from "./condominio";
import { Condomino as _Condomino } from "./condomino";
import type { CondominoAttributes, CondominoCreationAttributes } from "./condomino";
import { Fracoes as _Fracoes } from "./fracoes";
import type { FracoesAttributes, FracoesCreationAttributes } from "./fracoes";
import { GroupPermissions as _GroupPermissions } from "./group_permissions";
import type { GroupPermissionsAttributes, GroupPermissionsCreationAttributes } from "./group_permissions";
import { Groups as _Groups } from "./groups";
import type { GroupsAttributes, GroupsCreationAttributes } from "./groups";
import { Imagens as _Imagens } from "./imagens";
import type { ImagensAttributes, ImagensCreationAttributes } from "./imagens";
import { Movimentos as _Movimentos } from "./movimentos";
import type { MovimentosAttributes, MovimentosCreationAttributes } from "./movimentos";
import { Ocorrencias as _Ocorrencias } from "./ocorrencias";
import type { OcorrenciasAttributes, OcorrenciasCreationAttributes } from "./ocorrencias";
import { PagamentosQuotas as _PagamentosQuotas } from "./pagamentos_quotas";
import type { PagamentosQuotasAttributes, PagamentosQuotasCreationAttributes } from "./pagamentos_quotas";
import { Permissions as _Permissions } from "./permissions";
import type { PermissionsAttributes, PermissionsCreationAttributes } from "./permissions";
import { Quota as _Quota } from "./quota";
import type { QuotaAttributes, QuotaCreationAttributes } from "./quota";
import { TiposFracao as _TiposFracao } from "./tipos_fracao";
import type { TiposFracaoAttributes, TiposFracaoCreationAttributes } from "./tipos_fracao";

export {
  _Condominio as Condominio,
  _Condomino as Condomino,
  _Fracoes as Fracoes,
  _GroupPermissions as GroupPermissions,
  _Groups as Groups,
  _Imagens as Imagens,
  _Movimentos as Movimentos,
  _Ocorrencias as Ocorrencias,
  _PagamentosQuotas as PagamentosQuotas,
  _Permissions as Permissions,
  _Quota as Quota,
  _TiposFracao as TiposFracao,
};

export type {
  CondominioAttributes,
  CondominioCreationAttributes,
  CondominoAttributes,
  CondominoCreationAttributes,
  FracoesAttributes,
  FracoesCreationAttributes,
  GroupPermissionsAttributes,
  GroupPermissionsCreationAttributes,
  GroupsAttributes,
  GroupsCreationAttributes,
  ImagensAttributes,
  ImagensCreationAttributes,
  MovimentosAttributes,
  MovimentosCreationAttributes,
  OcorrenciasAttributes,
  OcorrenciasCreationAttributes,
  PagamentosQuotasAttributes,
  PagamentosQuotasCreationAttributes,
  PermissionsAttributes,
  PermissionsCreationAttributes,
  QuotaAttributes,
  QuotaCreationAttributes,
  TiposFracaoAttributes,
  TiposFracaoCreationAttributes,
};

export function initModels(sequelize: Sequelize) {
  const Condominio = _Condominio.initModel(sequelize);
  const Condomino = _Condomino.initModel(sequelize);
  const Fracoes = _Fracoes.initModel(sequelize);
  const GroupPermissions = _GroupPermissions.initModel(sequelize);
  const Groups = _Groups.initModel(sequelize);
  const Imagens = _Imagens.initModel(sequelize);
  const Movimentos = _Movimentos.initModel(sequelize);
  const Ocorrencias = _Ocorrencias.initModel(sequelize);
  const PagamentosQuotas = _PagamentosQuotas.initModel(sequelize);
  const Permissions = _Permissions.initModel(sequelize);
  const Quota = _Quota.initModel(sequelize);
  const TiposFracao = _TiposFracao.initModel(sequelize);

  Groups.belongsToMany(Permissions, { as: 'permission_id_permissions', through: GroupPermissions, foreignKey: "group_id", otherKey: "permission_id" });
  Permissions.belongsToMany(Groups, { as: 'group_id_groups', through: GroupPermissions, foreignKey: "permission_id", otherKey: "group_id" });
  Condomino.belongsTo(Condominio, { as: "id_condominio_condominio", foreignKey: "id_condominio"});
  Condominio.hasMany(Condomino, { as: "condominos", foreignKey: "id_condominio"});
  Fracoes.belongsTo(Condominio, { as: "id_condominio_condominio", foreignKey: "id_condominio"});
  Condominio.hasMany(Fracoes, { as: "fracos", foreignKey: "id_condominio"});
  Movimentos.belongsTo(Condominio, { as: "id_condominio_condominio", foreignKey: "id_condominio"});
  Condominio.hasMany(Movimentos, { as: "movimentos", foreignKey: "id_condominio"});
  Ocorrencias.belongsTo(Condominio, { as: "id_condominio_condominio", foreignKey: "id_condominio"});
  Condominio.hasMany(Ocorrencias, { as: "ocorrencia", foreignKey: "id_condominio"});
  Fracoes.belongsTo(Condomino, { as: "id_condomino_condomino", foreignKey: "id_condomino"});
  Condomino.hasMany(Fracoes, { as: "fracos", foreignKey: "id_condomino"});
  Movimentos.belongsTo(Condomino, { as: "id_condomino_condomino", foreignKey: "id_condomino"});
  Condomino.hasMany(Movimentos, { as: "movimentos", foreignKey: "id_condomino"});
  Ocorrencias.belongsTo(Condomino, { as: "autor_condomino", foreignKey: "autor"});
  Condomino.hasMany(Ocorrencias, { as: "ocorrencia", foreignKey: "autor"});
  PagamentosQuotas.belongsTo(Condomino, { as: "id_condomino_condomino", foreignKey: "id_condomino"});
  Condomino.hasMany(PagamentosQuotas, { as: "pagamentos_quota", foreignKey: "id_condomino"});
  Quota.belongsTo(Fracoes, { as: "id_condominio_fraco", foreignKey: "id_condominio"});
  Fracoes.hasMany(Quota, { as: "quota", foreignKey: "id_condominio"});
  Quota.belongsTo(Fracoes, { as: "id_fracao_fraco", foreignKey: "id_fracao"});
  Fracoes.hasMany(Quota, { as: "id_fracao_quota", foreignKey: "id_fracao"});
  Condominio.belongsTo(Groups, { as: "id_grupo_group", foreignKey: "id_grupo"});
  Groups.hasMany(Condominio, { as: "condominios", foreignKey: "id_grupo"});
  Condomino.belongsTo(Groups, { as: "id_grupo_group", foreignKey: "id_grupo"});
  Groups.hasMany(Condomino, { as: "condominos", foreignKey: "id_grupo"});
  GroupPermissions.belongsTo(Groups, { as: "group", foreignKey: "group_id"});
  Groups.hasMany(GroupPermissions, { as: "group_permissions", foreignKey: "group_id"});
  Imagens.belongsTo(Ocorrencias, { as: "id_condominio_ocorrencia", foreignKey: "id_condominio"});
  Ocorrencias.hasMany(Imagens, { as: "imagens", foreignKey: "id_condominio"});
  Imagens.belongsTo(Ocorrencias, { as: "id_ocorrencia_ocorrencia", foreignKey: "id_ocorrencia"});
  Ocorrencias.hasMany(Imagens, { as: "id_ocorrencia_imagens", foreignKey: "id_ocorrencia"});
  Quota.belongsTo(PagamentosQuotas, { as: "id_pagamento_pagamentos_quota", foreignKey: "id_pagamento"});
  PagamentosQuotas.hasMany(Quota, { as: "quota", foreignKey: "id_pagamento"});
  GroupPermissions.belongsTo(Permissions, { as: "permission", foreignKey: "permission_id"});
  Permissions.hasMany(GroupPermissions, { as: "group_permissions", foreignKey: "permission_id"});
  Fracoes.belongsTo(TiposFracao, { as: "tipo_tipos_fracao", foreignKey: "tipo"});
  TiposFracao.hasMany(Fracoes, { as: "fracos", foreignKey: "tipo"});

  return {
    Condominio: Condominio,
    Condomino: Condomino,
    Fracoes: Fracoes,
    GroupPermissions: GroupPermissions,
    Groups: Groups,
    Imagens: Imagens,
    Movimentos: Movimentos,
    Ocorrencias: Ocorrencias,
    PagamentosQuotas: PagamentosQuotas,
    Permissions: Permissions,
    Quota: Quota,
    TiposFracao: TiposFracao,
  };
}
