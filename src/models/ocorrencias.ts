import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Condominio, CondominioId } from './condominio';
import type { Condomino, CondominoId } from './condomino';
import type { Imagens, ImagensId } from './imagens';

export interface OcorrenciasAttributes {
  id: number;
  id_condominio: number;
  autor?: number;
  data_ocorrencia: string;
  titulo: string;
  descricao: string;
  info_adicional?: string;
  data_lim_resolucao: string;
  estado: string;
}

export type OcorrenciasPk = "id" | "id_condominio";
export type OcorrenciasId = Ocorrencias[OcorrenciasPk];
export type OcorrenciasOptionalAttributes = "autor" | "info_adicional" | "estado";
export type OcorrenciasCreationAttributes = Optional<OcorrenciasAttributes, OcorrenciasOptionalAttributes>;

export class Ocorrencias extends Model<OcorrenciasAttributes, OcorrenciasCreationAttributes> implements OcorrenciasAttributes {
  id!: number;
  id_condominio!: number;
  autor?: number;
  data_ocorrencia!: string;
  titulo!: string;
  descricao!: string;
  info_adicional?: string;
  data_lim_resolucao!: string;
  estado!: string;

  // Ocorrencias belongsTo Condominio via id_condominio
  id_condominio_condominio!: Condominio;
  getId_condominio_condominio!: Sequelize.BelongsToGetAssociationMixin<Condominio>;
  setId_condominio_condominio!: Sequelize.BelongsToSetAssociationMixin<Condominio, CondominioId>;
  createId_condominio_condominio!: Sequelize.BelongsToCreateAssociationMixin<Condominio>;
  // Ocorrencias belongsTo Condomino via autor
  autor_condomino!: Condomino;
  getAutor_condomino!: Sequelize.BelongsToGetAssociationMixin<Condomino>;
  setAutor_condomino!: Sequelize.BelongsToSetAssociationMixin<Condomino, CondominoId>;
  createAutor_condomino!: Sequelize.BelongsToCreateAssociationMixin<Condomino>;
  // Ocorrencias hasMany Imagens via id_condominio
  imagens!: Imagens[];
  getImagens!: Sequelize.HasManyGetAssociationsMixin<Imagens>;
  setImagens!: Sequelize.HasManySetAssociationsMixin<Imagens, ImagensId>;
  addImagen!: Sequelize.HasManyAddAssociationMixin<Imagens, ImagensId>;
  addImagens!: Sequelize.HasManyAddAssociationsMixin<Imagens, ImagensId>;
  createImagen!: Sequelize.HasManyCreateAssociationMixin<Imagens>;
  removeImagen!: Sequelize.HasManyRemoveAssociationMixin<Imagens, ImagensId>;
  removeImagens!: Sequelize.HasManyRemoveAssociationsMixin<Imagens, ImagensId>;
  hasImagen!: Sequelize.HasManyHasAssociationMixin<Imagens, ImagensId>;
  hasImagens!: Sequelize.HasManyHasAssociationsMixin<Imagens, ImagensId>;
  countImagens!: Sequelize.HasManyCountAssociationsMixin;
  // Ocorrencias hasMany Imagens via id_ocorrencia
  id_ocorrencia_imagens!: Imagens[];
  getId_ocorrencia_imagens!: Sequelize.HasManyGetAssociationsMixin<Imagens>;
  setId_ocorrencia_imagens!: Sequelize.HasManySetAssociationsMixin<Imagens, ImagensId>;
  addId_ocorrencia_imagen!: Sequelize.HasManyAddAssociationMixin<Imagens, ImagensId>;
  addId_ocorrencia_imagens!: Sequelize.HasManyAddAssociationsMixin<Imagens, ImagensId>;
  createId_ocorrencia_imagen!: Sequelize.HasManyCreateAssociationMixin<Imagens>;
  removeId_ocorrencia_imagen!: Sequelize.HasManyRemoveAssociationMixin<Imagens, ImagensId>;
  removeId_ocorrencia_imagens!: Sequelize.HasManyRemoveAssociationsMixin<Imagens, ImagensId>;
  hasId_ocorrencia_imagen!: Sequelize.HasManyHasAssociationMixin<Imagens, ImagensId>;
  hasId_ocorrencia_imagens!: Sequelize.HasManyHasAssociationsMixin<Imagens, ImagensId>;
  countId_ocorrencia_imagens!: Sequelize.HasManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Ocorrencias {
    return Ocorrencias.init({
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_condominio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'condominio',
        key: 'id'
      }
    },
    autor: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'condomino',
        key: 'id'
      }
    },
    data_ocorrencia: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    titulo: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    descricao: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    info_adicional: {
      type: DataTypes.STRING(500),
      allowNull: true,
      defaultValue: "NULL"
    },
    data_lim_resolucao: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    estado: {
      type: DataTypes.STRING(100),
      allowNull: false,
      defaultValue: "Pendente"
    }
  }, {
    sequelize,
    tableName: 'ocorrencias',
    schema: 'papv2',
    timestamps: false,
    indexes: [
      {
        name: "idx_16613_ocorrencias_fk",
        fields: [
          { name: "id_condominio" },
        ]
      },
      {
        name: "idx_16613_ocorrencias_fk_1",
        fields: [
          { name: "autor" },
        ]
      },
      {
        name: "idx_16613_primary",
        unique: true,
        fields: [
          { name: "id" },
          { name: "id_condominio" },
        ]
      },
    ]
  });
  }
}
