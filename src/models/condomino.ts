import * as Sequelize from "sequelize";
import { DataTypes, Model, Optional } from "sequelize";
import type { Condominio, CondominioId } from "./condominio";
import type { Fracoes, FracoesId } from "./fracoes";
import type { Groups, GroupsId } from "./groups";
import type { Ocorrencias, OcorrenciasId } from "./ocorrencias";
import bcrypt from "bcrypt";

export interface CondominoAttributes {
  id: number;
  nome_ocupante: string;
  nif_ocupante: number;
  telemovel_ocupante?: string;
  email_ocupante: string;
  password: string;
  id_condominio: number;
  id_grupo: number;
  auth_token?: string;
  username?: string;
  banido?: boolean;
}

export type CondominoPk = "id";
export type CondominoId = Condomino[CondominoPk];
export type CondominoOptionalAttributes = "id" | "telemovel_ocupante" | "id_grupo" | "auth_token" | "username";
export type CondominoCreationAttributes = Optional<CondominoAttributes, CondominoOptionalAttributes>;

export class Condomino extends Model<CondominoAttributes, CondominoCreationAttributes> implements CondominoAttributes {
  id!: number;
  nome_ocupante!: string;
  nif_ocupante!: number;
  telemovel_ocupante?: string;
  email_ocupante!: string;
  password!: string;
  id_condominio!: number;
  id_grupo!: number;
  auth_token?: string;
  username?: string;
  banido?: boolean;

  // Condomino belongsTo Condominio via id_condominio
  id_condominio_condominio!: Condominio;
  getId_condominio_condominio!: Sequelize.BelongsToGetAssociationMixin<Condominio>;
  setId_condominio_condominio!: Sequelize.BelongsToSetAssociationMixin<Condominio, CondominioId>;
  createId_condominio_condominio!: Sequelize.BelongsToCreateAssociationMixin<Condominio>;
  // Condomino hasMany Fracoes via id_condomino
  fracos!: Fracoes[];
  getFracos!: Sequelize.HasManyGetAssociationsMixin<Fracoes>;
  setFracos!: Sequelize.HasManySetAssociationsMixin<Fracoes, FracoesId>;
  addFraco!: Sequelize.HasManyAddAssociationMixin<Fracoes, FracoesId>;
  addFracos!: Sequelize.HasManyAddAssociationsMixin<Fracoes, FracoesId>;
  createFraco!: Sequelize.HasManyCreateAssociationMixin<Fracoes>;
  removeFraco!: Sequelize.HasManyRemoveAssociationMixin<Fracoes, FracoesId>;
  removeFracos!: Sequelize.HasManyRemoveAssociationsMixin<Fracoes, FracoesId>;
  hasFraco!: Sequelize.HasManyHasAssociationMixin<Fracoes, FracoesId>;
  hasFracos!: Sequelize.HasManyHasAssociationsMixin<Fracoes, FracoesId>;
  countFracos!: Sequelize.HasManyCountAssociationsMixin;
  // Condomino hasMany Ocorrencias via autor
  ocorrencia!: Ocorrencias[];
  getOcorrencia!: Sequelize.HasManyGetAssociationsMixin<Ocorrencias>;
  setOcorrencia!: Sequelize.HasManySetAssociationsMixin<Ocorrencias, OcorrenciasId>;
  addOcorrencium!: Sequelize.HasManyAddAssociationMixin<Ocorrencias, OcorrenciasId>;
  addOcorrencia!: Sequelize.HasManyAddAssociationsMixin<Ocorrencias, OcorrenciasId>;
  createOcorrencium!: Sequelize.HasManyCreateAssociationMixin<Ocorrencias>;
  removeOcorrencium!: Sequelize.HasManyRemoveAssociationMixin<Ocorrencias, OcorrenciasId>;
  removeOcorrencia!: Sequelize.HasManyRemoveAssociationsMixin<Ocorrencias, OcorrenciasId>;
  hasOcorrencium!: Sequelize.HasManyHasAssociationMixin<Ocorrencias, OcorrenciasId>;
  hasOcorrencia!: Sequelize.HasManyHasAssociationsMixin<Ocorrencias, OcorrenciasId>;
  countOcorrencia!: Sequelize.HasManyCountAssociationsMixin;
  // Condomino belongsTo Groups via id_grupo
  id_grupo_group!: Groups;
  getId_grupo_group!: Sequelize.BelongsToGetAssociationMixin<Groups>;
  setId_grupo_group!: Sequelize.BelongsToSetAssociationMixin<Groups, GroupsId>;
  createId_grupo_group!: Sequelize.BelongsToCreateAssociationMixin<Groups>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Condomino {
    return Condomino.init(
      {
        id: {
          autoIncrement: true,
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true,
        },
        nome_ocupante: {
          type: DataTypes.STRING(100),
          allowNull: false,
        },
        nif_ocupante: {
          type: DataTypes.DECIMAL,
          allowNull: false,
        },
        telemovel_ocupante: {
          type: DataTypes.STRING(15),
          allowNull: true,
          defaultValue: "NULL",
        },
        email_ocupante: {
          type: DataTypes.STRING(255),
          allowNull: false,
        },
        password: {
          type: DataTypes.STRING(255),
          allowNull: false,
        },
        id_condominio: {
          type: DataTypes.INTEGER,
          allowNull: false,
          references: {
            model: "condominio",
            key: "id",
          },
        },
        id_grupo: {
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 1,
          references: {
            model: "groups",
            key: "id",
          },
        },
        auth_token: {
          type: DataTypes.STRING(500),
          allowNull: true,
          defaultValue: "NULL",
        },
        username: {
          type: DataTypes.STRING(50),
          allowNull: true,
        },
        banido: {
          type: DataTypes.BOOLEAN,
          allowNull: true,
          defaultValue: "false",
        },
      },
      {
        hooks: {
          beforeCreate: (condominio) => {
            const salt = bcrypt.genSaltSync(); // gerar um salt (identificador)
            const hash = bcrypt.hashSync(condominio.getDataValue("password"), salt); // gerar a hash
            condominio.setDataValue("password", hash); // definir o valor da password para a hash
          },
        },
        sequelize,
        tableName: "condomino",
        schema: "papv2",
        timestamps: false,
        indexes: [
          {
            name: "idx_16588_condomino_fk",
            fields: [{ name: "id_grupo" }],
          },
          {
            name: "idx_16588_condomino_fk_1",
            fields: [{ name: "id_condominio" }],
          },
          {
            name: "idx_16588_primary",
            unique: true,
            fields: [{ name: "id" }],
          },
        ],
      },
    );
  }

  static async login(username: string, password: string) {
    // dar erro se o username estiver vazio
    if (!username) {
      throw new Error(LoginErrorCondomino.USERNAME_EMPTY);
    }

    // dar erro se a password estiver vazio
    if (!password) {
      throw new Error(LoginErrorCondomino.PASS_EMPTY);
    }

    // ver se existe um condomino com o username
    const condomino = await Condomino.findOne({
      where: { username },
      attributes: ["id", "id_grupo", "email_ocupante", "password"],
    });

    // dar erro se não existir um condomino
    if (!condomino) {
      throw new Error(LoginErrorCondomino.USER_NOT_EXISTS);
    }

    // comparar a hash das duas passwords (pedido , base de dados)
    const auth = bcrypt.compareSync(password, condomino.password);

    // se forem iguais devolver uma instância do condomino
    if (auth) {
      return condomino;
    }
    // dar erro se as passwords não forem iguais
    throw new Error(LoginErrorCondomino.PASS_WRONG);
  }

  static async saveToken(id: number, token: string) {
    // atualizar a coluna auth_token com o token gerado
    await Condomino.update({ auth_token: token }, { where: { id } });
  }

  static async removeToken(condomino: Condomino) {
    await Condomino.update({ auth_token: "" }, { where: { id: condomino.id } });
  }
}

export enum LoginErrorCondomino {
  USERNAME_EMPTY = "USERNAME_EMPTY",
  PASS_EMPTY = "PASS_EMPTY",
  USER_NOT_EXISTS = "USER_NOT_EXISTS",
  USER_BANNED = "USER_BANNED",
  PASS_WRONG = "PASS_WRONG",
}
