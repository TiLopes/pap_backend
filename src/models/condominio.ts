import * as Sequelize from "sequelize";
import { DataTypes, Model, Optional } from "sequelize";
import type { Condomino, CondominoId } from "./condomino";
import type { Fracoes, FracoesId } from "./fracoes";
import type { Groups, GroupsId } from "./groups";
import type { Ocorrencias, OcorrenciasId } from "./ocorrencias";
import bcrypt from "bcrypt";

export interface CondominioAttributes {
  id: number;
  nif: number;
  nome?: string;
  nome_admin?: string;
  email_admin?: string;
  telemovel_admin: string;
  morada: string;
  cod_postal: string;
  password: string;
  id_grupo: number;
  auth_token?: string;
  orcamento_anual: number;
  dia_pagamento_quota: number;
}

export type CondominioPk = "id";
export type CondominioId = Condominio[CondominioPk];
export type CondominioOptionalAttributes =
  | "id"
  | "nif"
  | "nome"
  | "nome_admin"
  | "email_admin"
  | "telemovel_admin"
  | "morada"
  | "cod_postal"
  | "id_grupo"
  | "auth_token"
  | "orcamento_anual"
  | "dia_pagamento_quota";
export type CondominioCreationAttributes = Optional<CondominioAttributes, CondominioOptionalAttributes>;

export class Condominio
  extends Model<CondominioAttributes, CondominioCreationAttributes>
  implements CondominioAttributes
{
  id!: number;
  nif!: number;
  nome?: string;
  nome_admin?: string;
  email_admin?: string;
  telemovel_admin!: string;
  morada!: string;
  cod_postal!: string;
  password!: string;
  id_grupo!: number;
  auth_token?: string;
  orcamento_anual!: number;
  dia_pagamento_quota!: number;

  // Condominio hasMany Condomino via id_condominio
  condominos!: Condomino[];
  getCondominos!: Sequelize.HasManyGetAssociationsMixin<Condomino>;
  setCondominos!: Sequelize.HasManySetAssociationsMixin<Condomino, CondominoId>;
  addCondomino!: Sequelize.HasManyAddAssociationMixin<Condomino, CondominoId>;
  addCondominos!: Sequelize.HasManyAddAssociationsMixin<Condomino, CondominoId>;
  createCondomino!: Sequelize.HasManyCreateAssociationMixin<Condomino>;
  removeCondomino!: Sequelize.HasManyRemoveAssociationMixin<Condomino, CondominoId>;
  removeCondominos!: Sequelize.HasManyRemoveAssociationsMixin<Condomino, CondominoId>;
  hasCondomino!: Sequelize.HasManyHasAssociationMixin<Condomino, CondominoId>;
  hasCondominos!: Sequelize.HasManyHasAssociationsMixin<Condomino, CondominoId>;
  countCondominos!: Sequelize.HasManyCountAssociationsMixin;
  // Condominio hasMany Fracoes via id_condominio
  fracos!: Fracoes[];
  getFracos!: Sequelize.HasManyGetAssociationsMixin<Fracoes>;
  setFracos!: Sequelize.HasManySetAssociationsMixin<Fracoes, FracoesId>;
  addFraco!: Sequelize.HasManyAddAssociationMixin<Fracoes, FracoesId>;
  addFracos!: Sequelize.HasManyAddAssociationsMixin<Fracoes, FracoesId>;
  createFraco!: Sequelize.HasManyCreateAssociationMixin<Fracoes>;
  removeFraco!: Sequelize.HasManyRemoveAssociationMixin<Fracoes, FracoesId>;
  removeFracos!: Sequelize.HasManyRemoveAssociationsMixin<Fracoes, FracoesId>;
  hasFraco!: Sequelize.HasManyHasAssociationMixin<Fracoes, FracoesId>;
  hasFracos!: Sequelize.HasManyHasAssociationsMixin<Fracoes, FracoesId>;
  countFracos!: Sequelize.HasManyCountAssociationsMixin;
  // Condominio hasMany Ocorrencias via id_condominio
  ocorrencia!: Ocorrencias[];
  getOcorrencia!: Sequelize.HasManyGetAssociationsMixin<Ocorrencias>;
  setOcorrencia!: Sequelize.HasManySetAssociationsMixin<Ocorrencias, OcorrenciasId>;
  addOcorrencium!: Sequelize.HasManyAddAssociationMixin<Ocorrencias, OcorrenciasId>;
  addOcorrencia!: Sequelize.HasManyAddAssociationsMixin<Ocorrencias, OcorrenciasId>;
  createOcorrencium!: Sequelize.HasManyCreateAssociationMixin<Ocorrencias>;
  removeOcorrencium!: Sequelize.HasManyRemoveAssociationMixin<Ocorrencias, OcorrenciasId>;
  removeOcorrencia!: Sequelize.HasManyRemoveAssociationsMixin<Ocorrencias, OcorrenciasId>;
  hasOcorrencium!: Sequelize.HasManyHasAssociationMixin<Ocorrencias, OcorrenciasId>;
  hasOcorrencia!: Sequelize.HasManyHasAssociationsMixin<Ocorrencias, OcorrenciasId>;
  countOcorrencia!: Sequelize.HasManyCountAssociationsMixin;
  // Condominio belongsTo Groups via id_grupo
  id_grupo_group!: Groups;
  getId_grupo_group!: Sequelize.BelongsToGetAssociationMixin<Groups>;
  setId_grupo_group!: Sequelize.BelongsToSetAssociationMixin<Groups, GroupsId>;
  createId_grupo_group!: Sequelize.BelongsToCreateAssociationMixin<Groups>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Condominio {
    return Condominio.init(
      {
        id: {
          autoIncrement: true,
          type: DataTypes.INTEGER,
          allowNull: false,
          primaryKey: true,
        },
        nif: {
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 0,
        },
        nome: {
          type: DataTypes.STRING(50),
          allowNull: true,
          defaultValue: "NULL",
        },
        nome_admin: {
          type: DataTypes.STRING(50),
          allowNull: true,
          defaultValue: "NULL",
        },
        email_admin: {
          type: DataTypes.STRING(50),
          allowNull: true,
          defaultValue: "NULL",
        },
        telemovel_admin: {
          type: DataTypes.STRING(13),
          allowNull: false,
          defaultValue: "0",
        },
        morada: {
          type: DataTypes.STRING(50),
          allowNull: false,
          defaultValue: "",
        },
        cod_postal: {
          type: DataTypes.STRING(8),
          allowNull: false,
          defaultValue: "0000-000",
        },
        password: {
          type: DataTypes.STRING(255),
          allowNull: false,
        },
        id_grupo: {
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 999,
          references: {
            model: "groups",
            key: "id",
          },
        },
        auth_token: {
          type: DataTypes.STRING(400),
          allowNull: true,
          defaultValue: "NULL",
        },
        orcamento_anual: {
          type: DataTypes.DECIMAL(10, 2),
          allowNull: false,
        },
        dia_pagamento_quota: {
          type: DataTypes.DECIMAL(2, 0),
          allowNull: true,
        },
      },
      {
        hooks: {
          beforeCreate: (condominio) => {
            /* Gerar um identificador da hash */
            const salt = bcrypt.genSaltSync();
            /* Gerar a hash dando o valor da password */
            const hash = bcrypt.hashSync(condominio.getDataValue("password"), salt);
            /* Definir o valor da password para o da hash */
            condominio.setDataValue("password", hash);
          },
        },
        sequelize,
        tableName: "condominio",
        schema: "papv2",
        timestamps: false,
        indexes: [
          {
            name: "idx_16572_cod_postal_unique",
            unique: true,
            fields: [{ name: "cod_postal" }],
          },
          {
            name: "idx_16572_fk_grupo",
            fields: [{ name: "id_grupo" }],
          },
          {
            name: "idx_16572_morada_unique",
            unique: true,
            fields: [{ name: "morada" }],
          },
          {
            name: "idx_16572_nif_unique",
            unique: true,
            fields: [{ name: "nif" }],
          },
          {
            name: "idx_16572_primary",
            unique: true,
            fields: [{ name: "id" }],
          },
        ],
      },
    );
  }

  static async login(email: string, password: string) {
    // dar erro se o email estiver vazio
    if (!email) {
      throw new Error(LoginErrorCondominio.EMAIL_EMPTY);
    }

    // dar erro se a password estiver vazio
    if (!password) {
      throw new Error(LoginErrorCondominio.PASS_EMPTY);
    }

    // ver se existe um condominio com o email
    const condominio = await Condominio.findOne({
      where: { email_admin: email },
      attributes: ["id", "id_grupo", "email_admin", "password"],
    });

    // dar erro se não existir um condomínio
    if (!condominio) {
      throw new Error(LoginErrorCondominio.USER_NOT_EXISTS);
    }

    // comparar a hash das duas passwords (pedido , base de dados)
    const auth = bcrypt.compareSync(password, condominio.password);

    // se forem iguais devolver uma instância do condomínio
    if (auth) {
      return condominio;
    }
    // dar erro se as passwords não forem iguais
    throw new Error(LoginErrorCondominio.PASS_WRONG);
  }

  static async saveToken(id: number, token: string) {
    // atualizar a coluna auth_token com o token gerado
    await Condominio.update({ auth_token: token }, { where: { id } });
  }

  static async removeToken(condominio: Condominio) {
    await Condominio.update({ auth_token: "" }, { where: { id: condominio.id } });
  }
}

export enum LoginErrorCondominio {
  EMAIL_EMPTY = "EMAIL_EMPTY",
  PASS_EMPTY = "PASS_EMPTY",
  USER_NOT_EXISTS = "USER_NOT_EXISTS",
  PASS_WRONG = "PASS_WRONG",
}
