import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Fracoes, FracoesId } from './fracoes';
import type { PagamentosQuotas, PagamentosQuotasId } from './pagamentos_quotas';

export interface QuotaAttributes {
  valor: number;
  data_vencimento?: string;
  frequencia: string;
  id: number;
  id_fracao: string;
  id_condominio: number;
  ativa?: boolean;
  data_inicio?: string;
  id_pagamento?: number;
}

export type QuotaPk = "id";
export type QuotaId = Quota[QuotaPk];
export type QuotaOptionalAttributes = "data_vencimento" | "frequencia" | "id" | "ativa" | "data_inicio" | "id_pagamento";
export type QuotaCreationAttributes = Optional<QuotaAttributes, QuotaOptionalAttributes>;

export class Quota extends Model<QuotaAttributes, QuotaCreationAttributes> implements QuotaAttributes {
  valor!: number;
  data_vencimento?: string;
  frequencia!: string;
  id!: number;
  id_fracao!: string;
  id_condominio!: number;
  ativa?: boolean;
  data_inicio?: string;
  id_pagamento?: number;

  // Quota belongsTo Fracoes via id_condominio
  id_condominio_fraco!: Fracoes;
  getId_condominio_fraco!: Sequelize.BelongsToGetAssociationMixin<Fracoes>;
  setId_condominio_fraco!: Sequelize.BelongsToSetAssociationMixin<Fracoes, FracoesId>;
  createId_condominio_fraco!: Sequelize.BelongsToCreateAssociationMixin<Fracoes>;
  // Quota belongsTo Fracoes via id_fracao
  id_fracao_fraco!: Fracoes;
  getId_fracao_fraco!: Sequelize.BelongsToGetAssociationMixin<Fracoes>;
  setId_fracao_fraco!: Sequelize.BelongsToSetAssociationMixin<Fracoes, FracoesId>;
  createId_fracao_fraco!: Sequelize.BelongsToCreateAssociationMixin<Fracoes>;
  // Quota belongsTo PagamentosQuotas via id_pagamento
  id_pagamento_pagamentos_quota!: PagamentosQuotas;
  getId_pagamento_pagamentos_quota!: Sequelize.BelongsToGetAssociationMixin<PagamentosQuotas>;
  setId_pagamento_pagamentos_quota!: Sequelize.BelongsToSetAssociationMixin<PagamentosQuotas, PagamentosQuotasId>;
  createId_pagamento_pagamentos_quota!: Sequelize.BelongsToCreateAssociationMixin<PagamentosQuotas>;

  static initModel(sequelize: Sequelize.Sequelize): typeof Quota {
    return Quota.init({
    valor: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    data_vencimento: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    frequencia: {
      type: DataTypes.STRING(20),
      allowNull: false,
      defaultValue: "Mensal"
    },
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    id_fracao: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'fracoes',
        key: 'id_condominio'
      }
    },
    id_condominio: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'fracoes',
        key: 'id_condominio'
      }
    },
    ativa: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: true
    },
    data_inicio: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    id_pagamento: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'pagamentos_quotas',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'quota',
    schema: 'papv2',
    timestamps: false,
    indexes: [
      {
        name: "quota_pkey",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  }
}
