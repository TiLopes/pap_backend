import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { GroupPermissions, GroupPermissionsId } from './group_permissions';
import type { Groups, GroupsId } from './groups';

export interface PermissionsAttributes {
  id: string;
  descricao: string;
}

export type PermissionsPk = "id";
export type PermissionsId = Permissions[PermissionsPk];
export type PermissionsCreationAttributes = PermissionsAttributes;

export class Permissions extends Model<PermissionsAttributes, PermissionsCreationAttributes> implements PermissionsAttributes {
  id!: string;
  descricao!: string;

  // Permissions hasMany GroupPermissions via permission_id
  group_permissions!: GroupPermissions[];
  getGroup_permissions!: Sequelize.HasManyGetAssociationsMixin<GroupPermissions>;
  setGroup_permissions!: Sequelize.HasManySetAssociationsMixin<GroupPermissions, GroupPermissionsId>;
  addGroup_permission!: Sequelize.HasManyAddAssociationMixin<GroupPermissions, GroupPermissionsId>;
  addGroup_permissions!: Sequelize.HasManyAddAssociationsMixin<GroupPermissions, GroupPermissionsId>;
  createGroup_permission!: Sequelize.HasManyCreateAssociationMixin<GroupPermissions>;
  removeGroup_permission!: Sequelize.HasManyRemoveAssociationMixin<GroupPermissions, GroupPermissionsId>;
  removeGroup_permissions!: Sequelize.HasManyRemoveAssociationsMixin<GroupPermissions, GroupPermissionsId>;
  hasGroup_permission!: Sequelize.HasManyHasAssociationMixin<GroupPermissions, GroupPermissionsId>;
  hasGroup_permissions!: Sequelize.HasManyHasAssociationsMixin<GroupPermissions, GroupPermissionsId>;
  countGroup_permissions!: Sequelize.HasManyCountAssociationsMixin;
  // Permissions belongsToMany Groups via permission_id and group_id
  group_id_groups!: Groups[];
  getGroup_id_groups!: Sequelize.BelongsToManyGetAssociationsMixin<Groups>;
  setGroup_id_groups!: Sequelize.BelongsToManySetAssociationsMixin<Groups, GroupsId>;
  addGroup_id_group!: Sequelize.BelongsToManyAddAssociationMixin<Groups, GroupsId>;
  addGroup_id_groups!: Sequelize.BelongsToManyAddAssociationsMixin<Groups, GroupsId>;
  createGroup_id_group!: Sequelize.BelongsToManyCreateAssociationMixin<Groups>;
  removeGroup_id_group!: Sequelize.BelongsToManyRemoveAssociationMixin<Groups, GroupsId>;
  removeGroup_id_groups!: Sequelize.BelongsToManyRemoveAssociationsMixin<Groups, GroupsId>;
  hasGroup_id_group!: Sequelize.BelongsToManyHasAssociationMixin<Groups, GroupsId>;
  hasGroup_id_groups!: Sequelize.BelongsToManyHasAssociationsMixin<Groups, GroupsId>;
  countGroup_id_groups!: Sequelize.BelongsToManyCountAssociationsMixin;

  static initModel(sequelize: Sequelize.Sequelize): typeof Permissions {
    return Permissions.init({
    id: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true
    },
    descricao: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'permissions',
    schema: 'papv2',
    timestamps: false,
    indexes: [
      {
        name: "idx_16623_description_unique",
        unique: true,
        fields: [
          { name: "descricao" },
        ]
      },
      {
        name: "idx_16623_primary",
        unique: true,
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  }
}
