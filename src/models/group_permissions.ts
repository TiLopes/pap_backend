import * as Sequelize from 'sequelize';
import { DataTypes, Model, Optional } from 'sequelize';
import type { Groups, GroupsId } from './groups';
import type { Permissions, PermissionsId } from './permissions';

export interface GroupPermissionsAttributes {
  group_id: number;
  permission_id: string;
}

export type GroupPermissionsPk = "group_id" | "permission_id";
export type GroupPermissionsId = GroupPermissions[GroupPermissionsPk];
export type GroupPermissionsCreationAttributes = GroupPermissionsAttributes;

export class GroupPermissions extends Model<GroupPermissionsAttributes, GroupPermissionsCreationAttributes> implements GroupPermissionsAttributes {
  group_id!: number;
  permission_id!: string;

  // GroupPermissions belongsTo Groups via group_id
  group!: Groups;
  getGroup!: Sequelize.BelongsToGetAssociationMixin<Groups>;
  setGroup!: Sequelize.BelongsToSetAssociationMixin<Groups, GroupsId>;
  createGroup!: Sequelize.BelongsToCreateAssociationMixin<Groups>;
  // GroupPermissions belongsTo Permissions via permission_id
  permission!: Permissions;
  getPermission!: Sequelize.BelongsToGetAssociationMixin<Permissions>;
  setPermission!: Sequelize.BelongsToSetAssociationMixin<Permissions, PermissionsId>;
  createPermission!: Sequelize.BelongsToCreateAssociationMixin<Permissions>;

  static initModel(sequelize: Sequelize.Sequelize): typeof GroupPermissions {
    return GroupPermissions.init({
    group_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'groups',
        key: 'id'
      }
    },
    permission_id: {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'permissions',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'group_permissions',
    schema: 'papv2',
    timestamps: false,
    indexes: [
      {
        name: "idx_16605_group_permissions_permission_id_group_id_unique",
        unique: true,
        fields: [
          { name: "group_id" },
          { name: "permission_id" },
        ]
      },
      {
        name: "idx_16605_permission_fk_idx",
        fields: [
          { name: "permission_id" },
        ]
      },
      {
        name: "idx_16605_primary",
        unique: true,
        fields: [
          { name: "group_id" },
          { name: "permission_id" },
        ]
      },
    ]
  });
  }
}
